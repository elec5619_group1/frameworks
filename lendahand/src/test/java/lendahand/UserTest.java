package lendahand;

import junit.framework.TestCase;
import au.group.users.model.User;

public class UserTest extends TestCase {

	private User user;

	// private User hibUser;
	// private UserService userService;

	protected void setUp() throws Exception {
		user = new User();
		// userService = new UserService();
	}

	/** Test the Model */
	public void testSetAndGetUserName() {
		String testUsername = "testUser";
		assertNull(user.getUsername());
		user.setUsername(testUsername);
		assertEquals(testUsername, user.getUsername());
	}

	public void testSetAndGetPassword() {
		String testPassword = "password";
		assertNull(user.getPassword());
		user.setPassword(testPassword);
		assertEquals(testPassword, user.getPassword());
	}

	public void testSetAndGetEmail() {
		String testEmail = "email";
		assertNull(user.getEmail());
		user.setEmail(testEmail);
		assertEquals(testEmail, user.getEmail());
	}

	public void testSetAndGetLoc() {
		String testLoc = "Sydney";
		assertNull(user.getLocation());
		user.setLocation(testLoc);
		assertEquals(testLoc, user.getLocation());
	}

	public void testSetAndGetDesc() {
		String testDesc = "I am a test.";
		assertNull(user.getDesc());
		user.setDesc(testDesc);
		assertEquals(testDesc, user.getDesc());
	}

	public void testSetAndGetFName() {
		String testFName = "Tester";
		assertNull(user.getFName());
		user.setFName(testFName);
		assertEquals(testFName, user.getFName());
	}

	public void testSetAndGetLName() {
		String testLName = "Guy";
		assertNull(user.getLName());
		user.setLName(testLName);
		assertEquals(testLName, user.getLName());
	}

	public void testSetAndGetProfileLink() {
		String testPLink = "www.imgur.com/testimage";
		assertNull(user.getProfileLink());
		user.setProfileLink(testPLink);
		assertEquals(testPLink, user.getProfileLink());
	}



}
