package lendahand;

import au.group.events.model.Event;
import au.group.social.model.UserConnection;
import au.group.social.service.SocialService;
import junit.framework.TestCase;

public class SocialTest extends TestCase{
	
	private UserConnection uCon;
	private SocialService socServ;
	
	/* Testing the model */
	
	protected void setUp() throws Exception {
		uCon = new UserConnection();
	}
	
	
	public void testSetAndGetUserId() {
		String testUsername = "testUser5321";
		assertNull(uCon.getUserId());
		uCon.setUserId(testUsername);
		assertEquals(testUsername, uCon.getUserId());
	}
	
	public void testSetAndGetProviderId() {
		String providerId = "FakeBook";
		assertNull(uCon.getProviderId());
		uCon.setProviderId(providerId);
		assertEquals(providerId, uCon.getProviderId());
	}
	/*
	public void testSetAndGetProviderUserId() {
		int providerUserId = 112288744;
		assertNull(uCon.getProviderUserId());
		uCon.setProviderUserId(providerUserId);
		assertEquals(providerUserId, uCon.getProviderUserId());
	}*/
	
	public void testSetAndGetDisplayName() {
		String displayName = "TimmyTonka";
		assertNull(uCon.getDisplayName());
		uCon.setDisplayName(displayName);
		assertEquals(displayName, uCon.getDisplayName());
	}
	
	public void testSetAndGetProfileUrl() {
		String profileUrl = "Test.url/2456";
		assertNull(uCon.getProfileUrl());
		uCon.setProfileUrl(profileUrl);
		assertEquals(profileUrl, uCon.getProfileUrl());
	}
	
	public void testSetAndGetImageUrl() {
		String imageUrl = "Test.url/56452456";
		assertNull(uCon.getImageUrl());
		uCon.setImageUrl(imageUrl);
		assertEquals(imageUrl, uCon.getImageUrl());
	}
	
	public void testSetAndGetAccessToken() {
		String accessToken = "sg;ih2308hefk3This is a very shortAccessTocken 232klnqw";
		assertNull(uCon.getAccessToken());
		uCon.setAccessToken(accessToken);
		assertEquals(accessToken, uCon.getAccessToken());
	}
	
	public void testSetAndGetSecret() {
		String secret = "Little Secret";
		assertNull(uCon.getSecret());
		uCon.setSecret(secret);
		assertEquals(secret, uCon.getSecret());
	}
	
	public void testSetAndGetRefreshToken() {
		String refreshToken = "Little refresh tok";
		assertNull(uCon.getRefreshToken());
		uCon.setRefreshToken(refreshToken);
		assertEquals(refreshToken, uCon.getRefreshToken());
	}
	
	public void testSetAndGetExpireTime() {
		String expireTime = "Little refresh tok";
		assertNull(uCon.getExpireTime());
		uCon.setExpireTime(expireTime);
		assertEquals(expireTime, uCon.getExpireTime());
	}
	/*
	public void testSetAndGetRank() {
		int rank = 123;
		assertNull(uCon.getRank());
		uCon.setRank(rank);
		assertEquals(rank, uCon.getRank());
	}*/
	
	/* Test for The Social Service */
	/*
	public void testServiceGetToken() {
		String name = "testUser5321";
		String accessToken = "sg;ih2308hefk3This is a very shortAccessTocken 232klnqw";
		assertEquals(accessToken, socServ.getToken(name));
	}*/
}
