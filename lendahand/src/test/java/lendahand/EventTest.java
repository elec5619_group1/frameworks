package lendahand;

import java.util.Date;

import junit.framework.TestCase;
import au.group.events.model.Event;
import au.group.users.model.User;

public class EventTest extends TestCase {

	private Event event;

	protected void setUp() throws Exception {
		event = new Event();
	}

	/** Test the Model */
	public void testSetAndGetEventName() {
		String testName = "testEvent";
		assertNull(event.getName());
		event.setName(testName);
		assertEquals(testName, event.getName());
	}
	
	public void testSetAndGetEventLocation() {
		String testLocation = "Sydney";
		assertNull(event.getLoc());
		event.setLoc(testLocation);
		assertEquals(testLocation, event.getLoc());
	}
	
	public void testSetAndGetEventDateCreated() {
		Date testDate = new Date();
		assertNull(event.getDateCreated());
		event.setDateCreated(testDate);
		assertEquals(testDate, event.getDateCreated());
	}
	
	public void testSetAndGetStartDate() {
		Date testDate = new Date();
		assertNull(event.getStartDate());
		event.setStartDate(testDate);
		assertEquals(testDate, event.getStartDate());
	}
	
	public void testSetAndGetDesc() {
		String testDesc = "This is a description";
		assertNull(event.getDesc());
		event.setDesc(testDesc);
		assertEquals(testDesc, event.getDesc());
	}
	
	public void testSetAndGetName() {
		String testName = "Test Event";
		assertNull(event.getName());
		event.setName(testName);
		assertEquals(testName, event.getName());
	}
	
	public void testSetAndGetCreatedBy() {
		User testUser = new User();
		assertNull(event.getCreatedBy());
		event.setCreatedBy(testUser);
		assertEquals(testUser, event.getCreatedBy());
	}
	
	public void testSetAndGetMaxVolunteers() {
		int testMax = 4;
		assertEquals(0, event.getMaxVolunteers());
		event.setMaxVolunteers(testMax);
		assertEquals(testMax, event.getMaxVolunteers());
	}
	
	public void testSetAndGetLat() {
		float testLat = 33.02f;
		assertEquals(0.0f, event.getLat());
		event.setLat(testLat);
		assertEquals(testLat, event.getLat());
	}
	
	public void testSetAndGetLng() {
		float testLng = 36.04f;
		assertEquals(0.0f, event.getLng());
		event.setLng(testLng);
		assertEquals(testLng, event.getLng());
	}


	/**
	 * Test the Service public void testUserDestroy() {
	 * 
	 * 
	 * String testUsername = "testUser"; User testUser =
	 * userService.getUser(testUsername); if (testUser == null){
	 * userService.createUser(testUsername, "pass", "test", "tester",
	 * "test@test.com", "TestCity"); }
	 * 
	 * userService.deleteUser(testUser); User reTestUser =
	 * userService.getUser(testUsername);
	 * 
	 * assertNull(reTestUser); }
	 */

}
