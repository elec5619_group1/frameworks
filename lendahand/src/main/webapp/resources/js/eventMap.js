var map;
var lat;
var lng;
var name;
function initialize() {
	model.forEach(function(entry) {
		lat = entry.lat;
		lng = entry.lng;
		name = entry.name;
	});
	console.log(lat);
	console.log(lng);
	var myLatlng = new google.maps.LatLng(lat, lng);
	/*
	 * var myLatlng = new google.maps.LatLng(
	 * google.loader.ClientLocation.latitude,
	 * google.loader.ClientLocation.longitude);
	 */
	var mapOptions = {
		zoom : 13,
		center : myLatlng
	}
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	addMarker();


}
google.maps.event.addDomListener(window, 'load', initialize);

function addMarker() {
	var latlng = new google.maps.LatLng(lat, lng);
	createMarker(latlng, name);
}

function createMarker(latlng, t) {
	var newmarker = new google.maps.Marker({
		position : latlng,
		map : map,
		title : t
	});

}