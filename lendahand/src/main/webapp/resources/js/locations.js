var map;
function initialize() {
	var myLatlng = new google.maps.LatLng(-33.876, 151.206);
	/*
	 * var myLatlng = new google.maps.LatLng(
	 * google.loader.ClientLocation.latitude,
	 * google.loader.ClientLocation.longitude);
	 */
	var mapOptions = {
		zoom : 12,
		center : myLatlng
	}
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	addAllMarkers();
	/*
	 * google.maps.event.addListener(map, 'click', function(event) {
	 * placeMarker(event.latLng); });
	 */

	google.maps.event.addListener(map, 'bounds_changed', function() {
		console.log("bounds changed");
	});
	google.maps.event.addListener(map, 'idle', function() {
		retrieveLocations(map.getBounds());
	});
	//console.log(context);
	google.maps.event.addListener(map, 'click', function(event) {
		var ll=event.latLng;
		console.log(event.latLng);
		var contentString = '<form name=\'createEventForm\' action=\'' +context+'/map/create'+ '\''
		+ 'method=\'GET\'>'
		+ '<table>'
		+	'<tr>'
		+		'<td>Event Name:</td>'
		+		'<td><input type=\'text\' name=\'name\'></td>'
		+	'</tr>'
		+	'<tr>'
		+		'<td>Description:</td>'
		+		'<td><input type=\'text\' name=\'desc\' /></td>'
		+	'</tr>'
		+	'<tr>'
		+		'<td>Start Date (DD-MM-YYYY):</td>'
		+		'<td><input type=\'date\' name=\'start\'></td>'
		+	'</tr>'
		+	'<tr>'
		+		'<td>End Date (DD-MM-YYYY):</td>'
		+		'<td><input type=\'date\' name=\'end\' /></td>'
		+	'</tr>'
		+	'<tr>'
		+		'<td>Address:</td>'
		+		'<td><input type=\'text\' name=\'location\' /></td>'
		+	'</tr>'
		+	'<tr>'
		+		'<td>Maximum Volunteers:</td>'
		+		'<td><input type=\'text\' name=\'maxV\' /></td>'
		+	'</tr>'
		+ '</table>'
		+ '<input name="createdBy" type="hidden" value='+ username+'>'
		+ '<input name="lat" type="hidden" value='+ ll.lat()+'>'
		+ '<input name="lng" type="hidden" value='+ ll.lng()+'>'
		+ '<input name="submit" type="submit" value="Create" class="button" />'
		+ '<input type="hidden" />'
	+ '</form>';
		var infoWindow = new google.maps.InfoWindow({
			content : contentString,
			maxWidth: 400
			
		});
		var newmarker = new google.maps.Marker({
			position : ll,
			map : map,
			title : 'New Event'
		});
		infoWindow.open(map,newmarker);
		google.maps.event.addListener(infoWindow,'closeclick',function(){
			newmarker.setMap(null);
		});
	});

}
google.maps.event.addDomListener(window, 'load', initialize);
// put function for getting markers for this new bounds here
function retrieveLocations(bounds) {
	console.log("new bounds: " + bounds);
}

function addAllMarkers() {
	availableEvents.forEach(function(entry) {
		var latlng = new google.maps.LatLng(entry.lat, entry.lng);
		var contentString = '<div id="infoBodyContent">' + '<div id="siteNotice">'
				+ '</div>' + '<h4>'+ entry.name	+ '</h4>'
				+ '<b>Event By: </b>'
				+ entry.createdBy+ '</p>'
				+ '<b>Description: </b>'
				+  '<p>' +entry.desc+ '</p>'
				+ '<b>Start Date: </b>'
				+ '<p>' +entry.start+ '</p>'
				+ '<b>End Date: </b>'
				+ '<p>' +entry.end+ '</p>'
				+ '<b>Maximum number of volunteers: </b>'
				+ entry.maxV +'</p>'
				//+ '<b>Current number of volunteers: </b>'
				//+ entry.currV+ '</p>'
				+ '</div>'
				+ '<form name=\'createEventForm\' action=\''  +context+'/map/join'+ '\''
				+ 'method=\'GET\'>'
				+ '<input name="name" type="hidden" value = ' + username+'>'
				+ '<input name="id" type="hidden" value='+ entry.id+'>'
				+ '<input name="join" type="submit" value="join" class="button" />'
				+ '<input type="hidden" />'
			+ '</form>';
		createMarker(latlng, contentString, entry.name);
	});
	usersEvents.forEach(function(entry) {
		var latlng = new google.maps.LatLng(entry.lat, entry.lng);
		var contentString = '<div id="infoBodyContent">' + '<div id="siteNotice">'
				+ '</div>' + '<h4>'+ entry.name	+ '</h4>'
				+ '<b>Event By: </b>'
				+ entry.createdBy+ '</p>'
				+ '<b>Description: </b>'
				+  '<p>' +entry.desc+ '</p>'
				+ '<b>Start Date: </b>'
				+ '<p>' +entry.start+ '</p>'
				+ '<b>End Date: </b>'
				+ '<p>' +entry.end+ '</p>'
				+ '<b>Maximum number of volunteers: </b>'
				+ entry.maxV +'</p>'
				//+ '<b>Current number of volunteers: </b>'
				//+ entry.currV+ '</p>'
				+ '</div>'
				+ '<form name=\'createEventForm\' action=\''  +context+'/map/deleteEvent'+ '\''
				+ 'method=\'GET\'>'
				+ '<input name="name" type="hidden" value = ' + username+'>'
				+ '<input name="id" type="hidden" value='+ entry.id+'>'
				+ '<input name="delete" type="submit" value="delete" class="button" />'
				+ '<input type="hidden" />'
			+ '</form>';
		createMarker(latlng, contentString, entry.name);
	});
	pledgedEvents.forEach(function(entry) {
		var latlng = new google.maps.LatLng(entry.lat, entry.lng);
		var contentString = '<div id="infoBodyContent">' + '<div id="siteNotice">'
				+ '</div>' + '<h4>'+ entry.name	+ '</h4>'
				+ '<b>Event By: </b>'
				+ entry.createdBy+ '</p>'
				+ '<b>Description: </b>'
				+  '<p>' +entry.desc+ '</p>'
				+ '<b>Start Date: </b>'
				+ '<p>' +entry.start+ '</p>'
				+ '<b>End Date: </b>'
				+ '<p>' +entry.end+ '</p>'
				+ '<b>Maximum number of volunteers: </b>'
				+ entry.maxV +'</p>'
				//+ '<b>Current number of volunteers: </b>'
				//+ entry.currV+ '</p>'
				+ '</div>'
				+ '<form name=\'createEventForm\' action=\'' +context+'/map/leave'+ '\''
				+ 'method=\'GET\'>'
				+ '<input name="name" type="hidden" value = ' + username+'>'
				+ '<input name="id" type="hidden" value='+ entry.id+'>'
				+ '<input name="leave" type="submit" value="leave" class="button" />'
				+ '<input type="hidden" />'
			+ '</form>';
		createMarker(latlng, contentString, entry.name);
	});
	
}

function createMarker(latlng, html, t) {
	var newmarker = new google.maps.Marker({
		position : latlng,
		map : map,
		title : t,
		//TODO: set this to true after testing with selenium
		optimized:false
	});

	newmarker['infowindow'] = new google.maps.InfoWindow({
		content : html,
		maxWidth: 300
	});

	google.maps.event.addListener(newmarker, 'click', function() {
		this['infowindow'].open(map, this);
	});
}

function getLatLong(address) {
	var geo = new google.maps.Geocoder;

	geo.geocode({
		'address' : address
	}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			return results[0].geometry.location;
		} else {
			alert("Geocode was not successful for the following reason: "
					+ status);
		}

	});

}
