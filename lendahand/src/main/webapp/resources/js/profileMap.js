/**
 * 
 */
var map;
function initialize() {
	var myLatlng = new google.maps.LatLng(-33.876, 151.206);
	/*
	 * var myLatlng = new google.maps.LatLng(
	 * google.loader.ClientLocation.latitude,
	 * google.loader.ClientLocation.longitude);
	 */
	var mapOptions = {
		zoom : 10,
		center : myLatlng
	}
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	addAllMarkers();


}
google.maps.event.addDomListener(window, 'load', initialize);

function addAllMarkers() {
	model.forEach(function(entry) {
		var latlng = new google.maps.LatLng(entry.lat, entry.lng);
		var contentString = '<div id="infoBodyContent">' + '<div id="siteNotice">'
				+ '</div>' + '<h4>'+ entry.name	+ '</h4>'
				+ '<b>Event By: </b>'
				+ entry.createdBy+ '</p>'
				+ '<b>Description: </b>'
				+  '<p>' +entry.desc+ '</p>'
				+ '<b>Start Date: </b>'
				+ '<p>' +entry.start+ '</p>'
				+ '<b>End Date: </b>'
				+ '<p>' +entry.end+ '</p>'
				+ '<b>Maximum number of volunteers: </b>'
				+ entry.maxV +'</p>'
				//+ '<b>Current number of volunteers: </b>'
				//+ entry.currV+ '</p>'
				+ '</div>';
		createMarker(latlng, contentString, entry.name);
	});
}

function createMarker(latlng, html, t) {
	var newmarker = new google.maps.Marker({
		position : latlng,
		map : map,
		title : t
	});

	newmarker['infowindow'] = new google.maps.InfoWindow({
		content : html,
		maxWidth: 300
	});

	google.maps.event.addListener(newmarker, 'click', function() {
		this['infowindow'].open(map, this);
	});
}