$("#shapeSelect").change( function () {
	updateShapeDisplay();
});

$("#colourSelect").change( function () {
	updateShapeDisplay();
});

$("#addShape").click(function(){
	var svg = d3.select("#badgeDisplay"); 
	appendShape(svg);
	
	$("#badgeSVG").val($("#badgeDisplay").html());
});

$("#undoShape").click(function(){
	var svg = d3.select("#badgeDisplay"); 
	var shapes = svg.selectAll("*")[0];
	var numNodes = shapes.length;
	if (numNodes > 0)
		shapes[numNodes-1].remove();
});

$(document).foundation({
  slider: {
    on_change: function(){
    	updateShapeDisplay();
    }
  }
});


function appendShape(svg)
{
	var shape;
	switch ($("#shapeSelect").val())
	{
	case "circle":
		shape = svg.append("circle").
			attr("r", 40);
		break;
	case "square":
		shape = svg.append("rect").
			attr("x", -30).
			attr("y", -30).
			attr("width", 60).
			attr("height", 60);
		break;
	case "cross":
		shape = svg.append("polygon").
			attr("points", "10,10 30,10 30,-10 10,-10 10,-30 -10,-30 -10,-10 -30,-10 -30,10 -10,10 -10,30 10,30");
		break;
	case "triangle":
		shape = svg.append("polygon")
		.attr("points", "30.0,0.0 -14.0,26.0 -15.0,-25.0");
		break
	case "star5":
		shape = svg.append("polygon")
		.attr("points","10.0,0.0 25.0,18.0 4.0,10.0 -9.0,29.0 -8.0,6.0 -30.0,1.0 -8.0,-5.0 -9.0,-28.0 4.0,-9.0 25.0,-17.0");
		break;
	case "star20":
		shape = svg.append("polygon")
		.attr("points", "10.0,0.0 30.0,5.0 10.0,4.0 27.0,14.0 9.0,6.0 22.0,22.0 6.0,9.0 14.0,27.0 4.0,10.0 5.0,30.0 1.0,10.0 -4.0,30.0 -3.0,10.0 -13.0,27.0 -5.0,9.0 -21.0,22.0 -8.0,6.0 -26.0,14.0 -9.0,4.0 -29.0,5.0 -10.0,1.0 -29.0,-4.0 -9.0,-3.0 -26.0,-13.0 -8.0,-5.0 -21.0,-21.0 -5.0,-8.0 -13.0,-26.0 -3.0,-9.0 -4.0,-29.0 -0.0,-10.0 5.0,-29.0 4.0,-9.0 14.0,-26.0 6.0,-8.0 22.0,-21.0 9.0,-5.0 27.0,-13.0 10.0,-3.0 30.0,-4.0");
		break;
	}
	
	shape.attr("fill", $("#colourSelect").val()).
	attr("stroke", "black").
	attr("stroke-width", 2).
	attr("transform", "translate(50,50) rotate("+$('#rotation').attr('data-slider')+") scale("
			+$('#scale').attr('data-slider')/100+")");
}

function updateShapeDisplay()
{
	var svg = d3.select("#shapeDisplay"); 
	
	svg.selectAll("*").remove();
	
	appendShape(svg);	
}

updateShapeDisplay();