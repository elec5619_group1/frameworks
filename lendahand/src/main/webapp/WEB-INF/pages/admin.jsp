<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">

</head>
<body>
	<h1>Administration</h1>
	<h3>Upgrade Users to Charities or remove a troublesome user.</h3>

	<c:url value="/logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<h5>
			<a href="javascript:formSubmit()"> Logout</a>
		</h5>
	</c:if>

	<span>${error}</span>

	<table>
		<thead>
			<tr>
				<th>Username</th>
				<th>Name</th>
				<th>Email</th>
				<th>Is Enabled</th>
				<th>Is a Charity</th>
			</tr>
		</thead>
		<tbody>

			<c:forEach items="${users}" var="user">
				<tr>
					<td>${user.username}</td>
					<td>${user.getFName()} ${user.getLName()}</td>
					<td>${user.getEmail()}</td>
					<td><c:choose>
							<c:when test="${user.isEnabled() == true}">
								<form name='unenable${user.username}'
									action="<c:url value='/admin/togEnabled' />" method='POST'>
									<input type='hidden' name='username' value="${user.username}" />
									<input name="remove" type="submit" value="Remove Enabled"
										class="button small alert" /> <input type="hidden"
										name="${_csrf.parameterName}" value="${_csrf.token}" />
								</form>
							</c:when>
							<c:otherwise>
								<form name='enable${user.username}'
									action="<c:url value='/admin/togEnabled' />" method='POST'>
									<input type='hidden' name='username' value="${user.username}" />
									<input name="add" type="submit" value="Add Enabled"
										class="button small" /> <input type="hidden"
										name="${_csrf.parameterName}" value="${_csrf.token}" />
								</form>
							</c:otherwise>
						</c:choose></td>
					<td><c:choose>
							<c:when test="${user.isCharity() == true}">
								<form name='toggle${user.username}'
									action="<c:url value='/admin/togCharity' />" method='POST'>
									<input type='hidden' name='username' value="${user.username}" />
									<input name="remove" type="submit" value="Remove Charity"
										class="button small alert" /> <input type="hidden"
										name="${_csrf.parameterName}" value="${_csrf.token}" />
								</form>
							</c:when>
							<c:otherwise>
								<form name='toggle${user.username}'
									action="<c:url value='/admin/togCharity' />" method='POST'>
									<input type='hidden' name='username' value="${user.username}" />
									<input name="add" type="submit" value="Add Charity"
										class="button small" /> <input type="hidden"
										name="${_csrf.parameterName}" value="${_csrf.token}" />
								</form>
							</c:otherwise>
						</c:choose></td>
				</tr>
			</c:forEach>

		</tbody>
	</table>




</body>
</html>