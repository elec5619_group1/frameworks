<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Share an Event on Facebook</title>
<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/foundation.min.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../topNav.jsp"></jsp:include>
	<div>
		<h3>Choose a job to share to friends</h3>
		<c:forEach var="event" items="${eventList}">
			<b><c:out value="${event.name}"></c:out></b><br>
			Description<br>
			<b><c:out value="${event.desc}"></c:out></b>
			<form action="<c:url value='social/post/event'/>" method="POST">
				<input name="submit" type="submit" value="Share ${event.name} on Facebook" class="button">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</form>
				
		</c:forEach>
	</div>
</body>
</html>