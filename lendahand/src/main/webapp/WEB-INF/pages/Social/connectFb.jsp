<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LendAHand || Connect to Facebook</title>
<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/foundation.min.css" />"
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../topNav.jsp"></jsp:include>
	<div align="center"
		style="margin-left: auto; margin-right: auto; width: 70%;">
		<h3>
			Hello,
			<c:out value="${data.name}" />
			!
		</h3>
		<div align="left">
			<p>Here is where you can share a post with your friends on
				Facebook about LandAHand</p>
			<p class="text-info">Type a message below to make a post to your
				Facebook Timeline, or just checkout what your friends are up to!!!</p>
		</div>
		<form name="statusUpdate" action="<c:url value='/social/post'/>"
			method="POST">
			<table class="table">
				<tr>
					<td><input class="span4" type="text" name="message"
						placeholder="A message for your friends..."></td>

					<td><input name="submit" type="submit"
						value="Tell your friends about LendaHand" class="button"></td>
				</tr>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
			</table>
		</form>
		<h4>
			<b>What your friends are saying</b>
		</h4>

		<div align="left">
			<c:forEach var="post" items="${feed}">
				<b> <c:out value="${post.from.name}" />
				</b> wrote:
			<p>
					<c:out value="${post.message}" />
				</p>
				<img src="${post.picture}" />
				<hr />
			</c:forEach>
		</div>
	</div>
</body>
</html>