<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
<title>LendAHand || Log In</title>

<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/foundation.min.css" />"
	rel="stylesheet">
</head>
<body onload='document.loginForm.username.focus();'>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}/">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<ul class="right">
				<li class="active"><a
					href="${pageContext.request.contextPath}/login">Log
						On</a></li>
				<li class="active"><a
					href="${pageContext.request.contextPath}/register">Register</a></li>

			</ul>

			<!-- Left Nav Section -->
			<ul class="left">
				<!-- Shouldn't have anything when not logged in, yo. -->
			</ul>
		</section>
	</nav>
	<br />

	<div class="row">
		<div class="large-12 columns">
			<img src="<c:url value="/resources/img/LendAHandLogo1.png" />">
			<br>
		</div>
	</div>
	<div align="center">
		<h5>Let's Start Giving Back, Have Some Fun...</h5>
		<h5>
			<strong><i>And Feel Great.</i></strong>
		</h5>
		<br>
	</div>
	<div align="center"
		style="margin-left: auto; margin-right: auto; width: 50%;">
		<p>Please login with your Username and Password</p>

		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</c:if>

		<form name='loginForm' action="<c:url value='/login' />" method='POST'>
			<table>
				<tr>
					<td>User:</td>
					<td><input type='text' name='username'></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input type='password' name='password' /></td>
				</tr>
			</table>
			<input name="submit" type="submit" value="Log In" class="button" />
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
	</div>

</body>
</html>