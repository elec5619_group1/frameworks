<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello Facebook</title>
<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/foundation.min.css" />"
	rel="stylesheet">
</head>
<body>
<jsp:include page="../topNav.jsp"></jsp:include>
<h3>Connect to Facebook</h3>

		<form action="<c:url value="/connect/facebook" />" method="POST">
			<input type="hidden" name="scope" value="read_stream,publish_stream,offline_access" />
			<div class="formInfo">
				<p>You aren't connected to Facebook yet. Click the button to connect this application with your Facebook account.</p>
			</div>
			<p><button type="submit">Connect to Facebook</button></p>
			<input type="hidden"
			name="${_csrf.parameterName}"
			value="${_csrf.token}"/>
		</form>
</body>
</html>