<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || ${user.username}</title>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/locations.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgexu0xK6ikbhRlkNLeIwdT1b4jiQ7Je0">
    </script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    	var username = "<c:out value="${user.username}" />";
    	var model=[];
    	<c:forEach items="${eventsList}" var="events">
		  var event=[];
		  console.log("id: "+${events.id});
		  event.id="<c:out value="${events.id}" />";
    	  event.lat="<c:out value="${events.lat}" />";
    	  event.lng="<c:out value="${events.lng}" />";
    	  event.name="<c:out value="${events.name}"/>";
    	  event.desc="<c:out value="${events.desc}"/>";
    	  event.loc="<c:out value="${events.loc}"/>";
    	  event.createdBy="<c:out value="${events.createdBy.username}"/>";
    	  event.start="<c:out value="${events.startDate}"/>";
    	  event.end="<c:out value="${events.endDate}"/>";
    	  event.maxV="<c:out value="${events.maxVolunteers}"/>";

    	  model.push(event);
    	</c:forEach>

    </script>
<script src="resources/js/profileMap.js"></script>
<link href="<c:url value="/resources/js/profileMap.js" />">
</head>
<style>
.primary {
	padding-left: 50px;
}
</style>
<body>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<sec:authorize access="isAnonymous()">
				<ul class="right">
					<li class="active"><a
						href="${pageContext.request.contextPath}/login">Log In</a></li>
				</ul>
			</sec:authorize>
			<sec:authorize access="isAuthenticated()">
				<ul class="right">
					<li class="active"><c:url value="/logout" var="logoutUrl" />
						<form action="${logoutUrl}" method="post" id="logoutForm">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</form> <script>
							function formSubmit() {
								document.getElementById("logoutForm").submit();
							}
						</script> <a href="javascript:formSubmit()">Log Off</a></li>
				</ul>
			</sec:authorize>




			<!-- Left Nav Section -->
		</section>
	</nav>
	<br />



	<div class="row">
		<div class="large-3 columns" align="center">
			<!-- User Profile Picture -->
			<c:choose>
				<c:when
					test="${user.getProfileLink() == null  || user.getProfileLink() == ''}">
					<img src="http://placehold.it/200x150&text=No+Profile+Picture+Set">
				</c:when>
				<c:otherwise>
					<img src="http://${user.getProfileLink()}"
						style="width: 200px; height: 150px">
				</c:otherwise>
			</c:choose>
			<!-- This will be tweaked when it comes to deployment -->
			<br />
			<div align="center">
				<span>External Profile Link: <br /> <a
					href="http://localhost:8080${pageContext.request.contextPath}/user/${user.username}"><small>http://localhost:8080${pageContext.request.contextPath}/user/${user.username}</small></a>
				</span>
			</div>
		</div>
		<div class="large-5 columns">
			<!-- User Welcome and short description -->
			<h3>${user.getFName()}${user.getLName()}</h3>
			<h5>
				<i>${user.username}</i>
			</h5>
			<h4>${user.getLocation()}</h4>
			<c:choose>
				<c:when test="${user.isCharity() == true}">
					<img src="<c:url value="/resources/img/certCharity.png" />"
						style="width: 350px; height: 80px">
				</c:when>
				<c:otherwise>
					<jsp:include page="../stats/stats_display.jsp" />
				</c:otherwise>
			</c:choose>
		</div>
		<div class="large-4 columns">
			<p>About Me</p>
			<p>
				<c:choose>
					<c:when test="${user.getDesc() == null }">Hey! ${user.getFName()} doesn't have an About Me yet. If you know them you should let them know we're waiting to hear more about them!</c:when>
					<c:otherwise>${user.getDesc()}</c:otherwise>
				</c:choose>
			</p>
		</div>
	</div>
	<br />
	<div class="row">
		<div class="large-7 columns">
			<!-- Karma System Dashboard -->
			<!--  <img src="http://placehold.it/550x300&text=Karma+Dashboard"> -->
			<span><i>Upcoming Causes</i></span>
			<table class="mini">
				<thead>
					<tr>
						<th width="20">Cause</th>
						<th width="20">Location</th>
						<th width="20">Coordinator</th>
						<th width="20">Date</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${joinedEvents.size() == 0}">
							<td>You haven't pledged to any Causes yet.</td>
						</c:when>
						<c:otherwise>
							<c:forEach items="${joinedEvents}" var="event">
								<tr class="mini">
									<td>${event.getName()}</td>
									<td>${event.getLoc()}</td>
									<td>${event.getCreatedBy().getUsername()}</td>
									<td>${event.getStartDate()}</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<span><i>Your Causes</i></span>
			<table>
				<thead>
					<tr>
						<th width="20">Cause</th>
						<th width="20">Location</th>
						<th width="20">Current Volunteers</th>
						<th width="20">Date</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${ownEvents.size() == 0}">
							<td>You haven't created to any Causes yet.</td>
						</c:when>
						<c:otherwise>
							<c:forEach items="${ownEvents}" var="event">
								<tr class="mini">
									<td>${event.getName()}</td>
									<td>${event.getLoc()}</td>
									<td>${event.getPledgedUsers().size()}</td>
									<td>${event.getStartDate()}</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<a class="right"
				href=${pageContext.request.contextPath}/events/eventlist>More</a>
		</div>
		<div class="large-5 columns" style="height: 400px">
			<div id="map-canvas"></div>
			<!-- Maps Dashboard -->
			<!--<img src="http://placehold.it/300x300&text=Map+of+Causes+Participated+In.">-->
		</div>
	</div>
	<br />
	<br />
</body>

</html>