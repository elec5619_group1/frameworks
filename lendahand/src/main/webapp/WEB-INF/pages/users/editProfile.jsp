<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || ${pageContext.request.userPrincipal.name}</title>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">
</head>
<style>
.primary {
	padding-left: 50px;
}
</style>
<body>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<ul class="right">
				<li class="active"><sec:authorize access="hasRole('ROLE_USER')">
						<c:url value="/logout" var="logoutUrl" />
						<form action="${logoutUrl}" method="post" id="logoutForm">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</form>
						<script>
							function formSubmit() {
								document.getElementById("logoutForm").submit();
							}
						</script>



						<a href="javascript:formSubmit()">Log Off</a>
					</sec:authorize></li>
			</ul>
		</section>
	</nav>
	<br />
	<div align="center"
		style="margin-left: auto; margin-right: auto; width: 50%;">
		<h3>Manage User</h3>

		<form name='editForm' action="<c:url value='/profile/edit' />"
			method='POST'>
			<table>
				<tr>
					<td>User:</td>
					<td>${user.username}</td>
				</tr>
				<tr>
					<td>Profile Picture: <br /> <small>Add a link to a
							profile picture. E.g. www.imgur.com/bRO2cEV.jpg
							<br/>Recommended: 200x150px</small></td>
					<td><input type='text' name='profilePic'
						value="${user.getProfileLink()}"></td>
				</tr>
				<tr>
					<td>Email:</td>
					<td><input type='email' name='email' value="${user.email}"></td>
				</tr>
				<tr>
					<td>Location:</td>
					<td><input type='text' name='location'
						value="${user.location}" /></td>
				</tr>
				<tr>
					<td>About Me:</td>
					<td><textarea rows="6" cols="2" name='description'
							placeholder="Write your about me in here and tell the LendAHand community about yourself :)">${user.getDesc()}</textarea></td>
				</tr>

			</table>
			<input name="submit" type="submit" value="Submit" class="button" />
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
	</div>
	<br>
</body>
</html>