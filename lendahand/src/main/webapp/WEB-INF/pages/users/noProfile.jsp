<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || No User Found</title>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">
</head>
<style>
.primary {
	padding-left: 50px;
}
</style>
<body>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<sec:authorize access="isAnonymous()">
				<ul class="right">
					<li class="active"><a
						href="${pageContext.request.contextPath}/login">Log In</a></li>
				</ul>
			</sec:authorize>
			<sec:authorize access="isAuthenticated()">
				<ul class="right">
					<li class="active"><c:url value="/logout" var="logoutUrl" />
						<form action="${logoutUrl}" method="post" id="logoutForm">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</form> <script>
							function formSubmit() {
								document.getElementById("logoutForm").submit();
							}
						</script> <a href="javascript:formSubmit()">Log Off</a></li>
				</ul>
			</sec:authorize>


			<!-- Left Nav Section -->
		</section>
	</nav>
	<br />
	<br />
	<h1>Uh-Oh!</h1>
	<h3>It looks like this user doesn't exist. Check and make sure
		you've got the right address.</h3>


</body>
</html>