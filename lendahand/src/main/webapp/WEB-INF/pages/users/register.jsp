<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
<title>LendAHand || Register</title>

<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/foundation.min.css" />"
	rel="stylesheet">

<style>
.errors {
	font-size: 80%;
	color: red;
}
</style>
</head>
<body onload='document.loginForm.username.focus();'>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}/">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<ul class="right">
				<li class="active"><a
					href="${pageContext.request.contextPath}/login">Log
						On</a></li>
				<li class="active"><a
					href="${pageContext.request.contextPath}/register">Register</a></li>

			</ul>

			<!-- Left Nav Section -->
			<ul class="left">
				<!-- Shouldn't have anything when not logged in, yo. -->
			</ul>
		</section>
	</nav>
	<br />
	<div align="center"
		style="margin-left: auto; margin-right: auto; width: 50%;">
		<h4>Be Awesome, Register!</h4>
		<p>
			<i>* = required</i>
		</p>
		<c:forEach items="${errors}" var="error">
			<p class="errors">${error.toString()}</p>
		</c:forEach>


		<form name='registerForm' action="<c:url value='/register' />"
			method='POST'>
			<table>
				<tr>
					<td>Username:*</td>
					<td><input type='text' name='username'></td>
				</tr>
				<tr>
					<td>Password:*</td>
					<td><input type='password' name='password' /></td>
				</tr>
				<tr>
					<td>Confirm Password:*</td>
					<td><input type='password' name='passwordConf' /></td>
				</tr>
				<tr>
					<td>First Name:</td>
					<td><input type='text' name='fName' /></td>
				</tr>
				<tr>
					<td>Last Name:</td>
					<td><input type='text' name='lName' /></td>
				</tr>
				<tr>
					<td>Email:*</td>
					<td><input type='email' name='email' /></td>
				</tr>
				<tr>
					<td>Location:</td>
					<td><input type='text' name='location' /></td>
				</tr>
			</table>
			<input name="submit" type="submit" value="Register" class="button" />
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
		</form>
	</div>

</body>
</html>