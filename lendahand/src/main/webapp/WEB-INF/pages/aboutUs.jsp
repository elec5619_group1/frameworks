<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || Start Giving Back</title>

<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/foundation.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">
</head>
<body>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}/">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<ul class="right">
				<li class="active"><a
					href="${requestScope['javax.servlet.forward.request_uri']}login">Log
						On</a></li>
				<li class="active"><a
					href="${requestScope['javax.servlet.forward.request_uri']}register">Register</a></li>

			</ul>

			<!-- Left Nav Section -->
			<ul class="left">
				<!-- Shouldn't have anything when not logged in, yo. -->
			</ul>
		</section>
	</nav>
	<br />


	<div class="row">
		<div class="large-12 columns">
			<img src="<c:url value="/resources/img/LendAHandLogo1.png" />">
			<br> <br>
		</div>
	</div>


	<div class="row">
		<div class="large-12 columns">
			<h1>About Us</h1>
			<br>
			<p>We're a small team of students currently studying the ELEC5619
				course at Sydney University. We hope to engage others and achieve
				relevance in positive computing through enabling others to engage in
				altruism and volunteering. We believe by promoting helping others
				and rewarding them extrinsically through arbitrary scores and
				points, they will reach a point where they realize their personal
				intrinsic value for helping out such causes.</p>
		</div>
	</div>


	<div class="row">
		<div class="large-12 columns">
			<a href="${pageContext.request.contextPath}/"
				class="small button radius">Back</a>
		</div>
	</div>

</body>
</html>

