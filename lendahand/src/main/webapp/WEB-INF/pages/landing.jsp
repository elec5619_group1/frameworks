<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || Start Giving Back</title>

<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/foundation.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">
</head>
<body>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}/">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<ul class="right">
				<li class="active"><a
					href="${pageContext.request.contextPath}/login">Log
						On</a></li>
				<li class="active"><a
					href="${pageContext.request.contextPath}/register">Register</a></li>

			</ul>

			<!-- Left Nav Section -->
			<ul class="left">
				<!-- Shouldn't have anything when not logged in, yo. -->
			</ul>
		</section>
	</nav>
	<br />


	<div class="row">
		<div class="large-12 columns">
			<img src="<c:url value="/resources/img/LendAHandLogo1.png" />">
			<br> <br>
		</div>
	</div>



	<div class="row">

		<div class="large-3 panel columns">
			<img src="<c:url value="/resources/img/volunteer.jpg" />">

			<h4>Why Volunteer?</h4>

			<p>This project hopes to leverage the altruistic and
				philanthropic tendencies of the people to help achieve the effect of
				positive computing. But why should we volunteer in the first place?</p>
			<hr>

			<div class="row">
				<div class="large-4 columns">
					<a href="${pageContext.request.contextPath}/whyvolunteer" class="small button">More on Volunteering?</a>
				</div>
			</div>
		</div>

		<div class="large-9 columns">
			<div class="panel">
				<div class="row">
					<h3>Find Causes Around You</h3>
					<p>Find Causes around your location that need your help! If you
						need help with something yourself, but it online and let the world
						know you need some people to lend a hand.</p>
				</div>

				<div class="row">
					<h3>Get Points for Helping</h3>
					<p>For every cause you participate in, you get points and
						badges!</p>
				</div>

				<div class="row">
					<h3>Show off your giving side!</h3>
					<p>Link up your lendahand profile to others to let them know
						you're an altruistic person. Attach it to your social network or
						resume and let the world know!</p>
				</div>

			</div>

			<div class="row">
				<div class="large-6 columns">
					<div class="panel">
						<h5>About Us</h5>
						<p>We're a small team of students currently studying the
							ELEC5619 course at Sydney University. We hope to engage others
							and achieve relevance in positive computing through enabling
							others to engage in altruism and volunteering</p>
						<a
							href="${requestScope['javax.servlet.forward.request_uri']}about"
							class="small button">About Us</a>
					</div>
				</div>

				<div class="large-6 columns">
					<div class="panel">
						<h5>ELEC5619</h5>
						<p>Sydney University's ELEC5619 - Object Oriented Application
							Frameworks introduces students to the main issues involved in
							producing large Internet systems by using and building
							application frameworks. The unit is currently taught by Rafael A.
							Calvo</p>
						<a
							href="http://sydney.edu.au/engineering/latte/teaching/elec5619.shtml"
							class="small button">More on ELEC5619</a>
					</div>
				</div>
			</div>
		</div>

	</div>



	<footer class="row">
		<div class="large-12 columns">
			<hr />
			<div class="row">
				<div class="large-6 columns">
					<p>2014, Semester 2</p>
				</div>

				<div class="large-6 columns">
					<p>ELEC5619 - Object Oriented Application Frameworks</p>
				</div>

			</div>
		</div>
	</footer>

</body>
</html>

