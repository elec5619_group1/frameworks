<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
	<link href="<c:url value="/resources/css/stats.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">


</head>
<body>

<div class="row">
	<div class="small-3 columns">
		<img src="<c:url value="/resources/img/eternal_glyph.png"/>" class="symbol" title="Eternal Karma measures how much you have helped others overall"/>
	</div>
	
	<div class="helper-text columns small-3">
		<c:out value="${score}"/>
	</div>
	
	<div class="small-3 columns">
		<img src="<c:url value="/resources/img/transient_glyph.png"/>" class="symbol" title="Transient Karma measures how much you have helped others recently"/>
	</div>
	
	<div class="helper-text columns small-3">
		<c:out value="${scoreRecent}"/>
	</div>
</div>


</body>
</html>