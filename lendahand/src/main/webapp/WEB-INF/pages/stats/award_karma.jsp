<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || ${pageContext.request.userPrincipal.name}</title>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
	<link href="<c:url value="/resources/css/stats.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.js" />"></script>
<script src="<c:url value="/resources/js/foundation.min.js" />"></script>
<script src="<c:url value="/resources/js/d3.min.js" />"></script>


</head>
<body>
<!-- Top navigation bar -->
<jsp:include page="../topNav.jsp" />

<h1>Award Karma</h1>

<h3>Who lent a hand?</h3>


	<form id="userList" role="form" method="post" action="">
		<div class="row section">
			<div class="column small-8">
				<label>Badge:</label>
			</div>
			<div class="column small-2">
				<c:if test="${stat.badges.size() > 0}">
					<select name="badge-selection" id="badge-selection">
						<c:forEach items="${stat.badges}" var="badge">
							<option value="${badge.badgeId}">${badge.name}</option>
						</c:forEach>
					</select>
				</c:if>
			</div>
		<c:if test="${stat.badges.size() > 0}">
		</c:if>
		</div>
			<c:forEach items="${cause.pledgedUsers}" var="user">
				<div class="row section">
					<div class="column small-6">
						<c:out value="${user.username}"/>
					</div>
					<div class ="column small-1">
						<c:if test="${stat.badges.size() > 0}">
							<c:forEach items="${stat.badges}" var="badge">
								<svg width=100 height=100 class="badge" display="hidden" name="${badge.badgeId}">${badge.badgeSVG}</svg>
							</c:forEach>
							<script>
							function showBadge()
							{
								$(".badge").attr("display", "hidden");
								$("[name="+$("#badge-selection").val()+"]").attr("display", "inline");
							}
							$("#badge-selection").change(showBadge);
							
							showBadge();
							</script>
						</c:if>
					</div>
					<div class ="column small-2">
						<c:if test="${stat.badges.size() > 0}">
							<div class ="switch large">
								<input type="checkbox" name="${user.username}_badge" id="${user.username}_badge"> 
								<label for="${user.username}_badge"></label>
							</div>
						</c:if>
					</div>
					<div class ="column small-1">
						<img src="<c:url value="/resources/img/eternal_glyph.png"/>" class="glyph"/>
					</div>
					<div class ="column small-2">
						<div class ="switch large">
							<input type="checkbox" name="${user.username}" id="${user.username}"> 
							<label for="${user.username}"></label>
						</div>
					</div>
				</div>
				
			</c:forEach>
		
		<div class="row"><div class="column small-12"><input type="submit" value="Done" class="button expand"></div></div>
		
		<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
	</form>


</body>
</html>