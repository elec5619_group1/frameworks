<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || ${pageContext.request.userPrincipal.name}</title>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
	<link href="<c:url value="/resources/css/stats.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">


</head>
<body>
<!-- Top navigation bar -->
<jsp:include page="../topNav.jsp" />

<h1>Stats</h1>
<h3>Karma</h3>
<div class="row section">
	<div class="small-2 columns">
		<img src="<c:url value="/resources/img/eternal_glyph.png"/>" class="symbol"/>
		<span class="score"><c:out value="${score}"/></span>
	</div>
	
	<div class="helper-text columns small-10">
		Eternal Karma measures how much you have helped others overall
	</div>
</div>

<div class="row section">
	<div class="small-2 columns">
		<img src="<c:url value="/resources/img/transient_glyph.png"/>" class="symbol"/>
		<span class="score"><c:out value="${scoreRecent}"/></span>
	</div>
	
	<div class="helper-text columns small-10">
		Transient Karma measures how much you have helped others recently
	</div>
</div>

</body>
</html>