<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || ${pageContext.request.userPrincipal.name}</title>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
	<link href="<c:url value="/resources/css/stats.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.js" />"></script>
<script src="<c:url value="/resources/js/foundation.min.js" />"></script>
<script src="<c:url value="/resources/js/d3.min.js" />"></script>


</head>
<body>
<!-- Top navigation bar -->
<jsp:include page="../topNav.jsp" />

<h1>Create a Badge</h1>

	<div class="row section">
	
		<div class='columns small-2'></div>
		<div class="columns small-10">
			<div style="text-align:center;">
				<svg width=100 height=100 id ='badgeDisplay' class="viewport"></svg>
			</div>
		</div>
		
		<div class='columns small-2'>
			<button class="button expand" id="undoShape">Remove Shape</button>
		</div>
	</div>
	
	<div class="row section">
		<div class='columns small-8'>
			<div class="row section">
				<div class='columns small-6'>
					<select id='shapeSelect'>
					  <option value="square">Square</option>
					  <option value="circle">Circle</option>
					  <option value="cross">Cross</option>
					  <option value="triangle">Triangle</option>
					  <option value="star5">5 Pointed Star</option>
					  <option value="star20">20 Pointed Star</option>
					</select>
				</div>
				
				<div class='columns small-6'>
					<select id='colourSelect'>
						<option value="black">Black</option>
						<option value=blue>Blue</option>
						<option value="green">Green</option>
						<option value="red">Red</option>
						<option value="orange">Orange</option>
						<option value="purple">Purple</option>
						<option value="yellow">Yellow</option>
						<option value="white">White</option>
					</select>
				</div>
			</div>
			
			<div class="row section">
				<div class='columns small-6'>
					<label>Rotate</label>
					<div class="range-slider" id="rotation" data-slider data-options="start: 0; end: 360;">
					  <span class="range-slider-handle" role="slider" tabindex="0"></span>
					  <span class="range-slider-active-segment"></span>
					  <input type="hidden">
					</div>
				</div>
				
				<div class='columns small-6'>
					<label>Size</label>
					<div class="range-slider" id="scale" data-slider data-options="start: 1; end: 150;">
					  <span class="range-slider-handle" role="slider" tabindex="0"></span>
					  <span class="range-slider-active-segment"></span>
					  <input type="hidden">
					</div>
				</div>
			</div>
		</div>
		
		<div class='columns small-2'>
			<div style="text-align:center;">
				<svg width=100 height=100 id ='shapeDisplay' class="viewport"></svg>
			</div>
		</div>
		
		<div class='columns small-2'>
			<button class="button expand" id="addShape">Add</button>
		</div>
	</div>
		
	<div class="row section">
		<div class="columns small-12">
			<form id="badge_design" role="form" method="post" action="">
				<input type="hidden" name="badgeSVG" id="badgeSVG" />
				<label for="name">Name</label>
				<input type="text" name="name" id="badge_name" />
				<div class="row"><div class="column small-2"><input type="submit" value="Done" class="button expand"></div></div>
				
				<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
			</form>
		</div>
	</div>

<script src="<c:url value="/resources/js/badges.js" />"></script>

</body>
</html>