<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || ${pageContext.request.userPrincipal.name}</title>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
	<link href="<c:url value="/resources/css/stats.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">


</head>
<body>
<!-- Top navigation bar -->
<jsp:include page="../topNav.jsp" />

<h1>Award Karma</h1>

<h3>People who lent a hand:</h3>

<c:forEach items="${cause.awardedUsers}" var="karma">
	<div class="row section">
		<div class="columns small-10">
			<c:out value="${karma.stat.user.username}"/>
		</div>
		<div class="small-2 columns">
			<img src="<c:url value="/resources/img/eternal_glyph.png"/>" class="glyph"/>
			<span class="score">+10</span>
		</div>
	</div>
</c:forEach>

<c:if test="${badge != null}">
<h3>People who earned a badge!:</h3>

<c:forEach items="${badgeEarners}" var="stat">
	<div class="row section">
		<div class="columns small-10">
			<c:out value="${stat.user.username}"/>
		</div>
		<div class="small-2 columns">
			<svg width=100 height=100>${badge.badgeSVG}</svg>
		</div>
	</div>
</c:forEach>
</c:if>

<a href=${pageContext.request.contextPath}/events/eventlist>Back to Causes</a>
</body>
</html>