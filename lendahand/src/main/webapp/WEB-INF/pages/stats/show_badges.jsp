<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || ${pageContext.request.userPrincipal.name}</title>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
	<link href="<c:url value="/resources/css/stats.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">

</head>
<body>
<!-- Top navigation bar -->
<jsp:include page="../topNav.jsp" />

<h1>Badges</h1>

<c:forEach items="${stat.badges}" var="badge">
	<div class="row section">
		<div class="columns small-6">
			<c:out value="${badge.name}"/>
		</div>
		<div class="small-2 columns">
			<svg height=100 width=100>${badge.badgeSVG}</svg>
		</div>
	</div>
</c:forEach>

</body>
</html>