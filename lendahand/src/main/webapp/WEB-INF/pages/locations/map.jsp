<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || Maps</title>


<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
	<link href="<c:url value="/resources/css/locations.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">

    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgexu0xK6ikbhRlkNLeIwdT1b4jiQ7Je0">
    </script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    	var username = "<c:out value="${pageContext.request.userPrincipal.name}" />";
    	var context = "<c:out value=" ${pageContext.request.contextPath}" />";
    	console.log(context);
    	var model=[];
    	var usersEvents=[];
    	var pledgedEvents=[];
    	var availableEvents=[];
    	<c:forEach items="${eventsList}" var="events">
		  var event=[];
		  console.log("id: "+${events.id});
		  event.id="<c:out value="${events.id}" />";
    	  event.lat="<c:out value="${events.lat}" />";
    	  event.lng="<c:out value="${events.lng}" />";
    	  event.name="<c:out value="${events.name}"/>";
    	  event.desc="<c:out value="${events.desc}"/>";
    	  event.loc="<c:out value="${events.loc}"/>";
    	  event.createdBy="<c:out value="${events.createdBy.username}"/>";
    	  event.start="<c:out value="${events.startDate}"/>";
    	  event.end="<c:out value="${events.endDate}"/>";
    	  event.maxV="<c:out value="${events.maxVolunteers}"/>";
    	  console.log(event.name);
    	  <c:choose>
		  	<c:when test="${events.createdBy.getUsername()==pageContext.request.userPrincipal.name}">
		  		usersEvents.push(event);
		  		console.log("Adding " +event.name +" to users events");
		  	</c:when>
		  	<c:otherwise>
		  	<c:set var="contains" value="false" />
				<c:forEach var="item" items="${events.pledgedUsers}">
								//${item.getUsername()}
					<c:if test="${item.getUsername() eq pageContext.request.userPrincipal.name}">
						<c:set var="contains" value="true" />
					</c:if>
				</c:forEach>
				<c:choose>
		  			<c:when test="${contains == true}">
		  				pledgedEvents.push(event);
		  			</c:when>
		  			<c:otherwise>
		  				availableEvents.push(event);
		  			</c:otherwise>
		  		</c:choose>
		  	</c:otherwise>
		  </c:choose>
		  
    	</c:forEach>
    	//var actURL = <c:url value= '/map/create'/>;
    </script>
      <script src="resources/js/locations.js"></script>
<link href="<c:url value="/resources/js/locations.js" />">

</head>
<body>
<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}/">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<ul class="right">
				<li class="active"><sec:authorize access="hasRole('ROLE_USER')">
						<c:url value="/logout" var="logoutUrl" />
						<form action="${logoutUrl}" method="post" id="logoutForm">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</form>
						<script>
							function formSubmit() {
								document.getElementById("logoutForm").submit();
							}
						</script>



						<a href="javascript:formSubmit()">Log Off</a>
					</sec:authorize></li>


			</ul>

			<!-- Left Nav Section -->
			<ul class="left">
				<li><a href=${pageContext.request.contextPath}/profile>Profile</a></li>
				<li><a href=${pageContext.request.contextPath}/events/eventlist>Causes</a></li>
				<li><a href=${pageContext.request.contextPath}/stats>Stats</a></li>
				<li><a href=${pageContext.request.contextPath}/connect/facebook>Social</a></li>
				<li><a href=${pageContext.request.contextPath}/map>Map</a></li>
			</ul>
		</section>
	</nav>
	<br />
<div id= "centred" style="width:70%">
</br>
<p>If you would like to create a new cause, just click on the map where the cause will take place.</p>
</div>
<!-- Map Canvas -->
<div id = "centred" style="height:70%; width:70%;">
<div id="map-canvas"></div>
</div>
	<br>
</body>
</html>