<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || Start Giving Back</title>

<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/foundation.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/js/foundation.min.js" />">
<link href="<c:url value="/resources/js/jquery.js" />">
</head>
<body>
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}/">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<ul class="right">
				<li class="active"><a
					href="${requestScope['javax.servlet.forward.request_uri']}login">Log
						On</a></li>
				<li class="active"><a
					href="${requestScope['javax.servlet.forward.request_uri']}register">Register</a></li>

			</ul>

			<!-- Left Nav Section -->
			<ul class="left">
				<!-- Shouldn't have anything when not logged in, yo. -->
			</ul>
		</section>
	</nav>
	<br />


	<div class="row">
		<div class="large-12 columns">
			<img src="<c:url value="/resources/img/LendAHandLogo1.png" />">
			<br> <br>
		</div>
	</div>


	<div class="row">
		<div class="large-12 columns">
			<h1>Why Volunteer?</h1>
			<br>
			<p>This project hopes to leverage the altruistic and
				philanthropic tendencies of the people to help achieve the effect of
				positive computing. Positive Computing is a relatively new notion,
				described as 'the study and development of technologies designed to
				support well-being, wisdom, and human potential' (Calvo & Peters,
				2012). Positive Computing focuses on enabling the user to foster or
				achieve optimal distinction in the personal, emotional or physical
				development of ones self or another; because of this Positive
				Computing can be easily associated with the two terms previously
				mentioned (Altruism and Philanthropy), opening the doors to many
				opportunities to promote a constructive environment and community
				for everyone in society. Altruism and Philanthropy, in contrast to
				Positive Computing, aren’t relatively new concepts, and a simple
				Google search will show a rather large number of forums, discussions
				and pages seeking to promote and incite discussion on the topic.
				Most people consider themselves to be fairly altruistic within the
				confines to how generous they can be with their time and finances.
				They also often do so for good and selfless reasons that live up to
				the common definition of Altruism. Counter-intuitive to the
				definition, individuals can also be altruistic to benefit their own
				self-development, their community and even their health (Post, 2007)</p>

			<h4>Why is the theme of Altruism important? Because Helping is
				Good for you!</h4>
			<p>Through researching the field, Stephen G. Post has stood out
				as a prolific figure in topic of Altruism and an individual’s
				wellbeing, health and happiness. Post (2008) describes in his
				article, ‘Happiness, Health and Altruism’, that Altruism results in
				‘a deeper and more positive social integration’, distracting one’s
				self from their own issues and finding a more active lifestyle
				rather than an isolated passive one. Benefits of volunteerism for
				Mental health are also quite prevalent and worth taking note, with
				the noted reduction in depressive symptoms, elevated happiness and
				social skills. Following Post’s material on Altruism and an
				individual’s wellbeing, there stands an impression that by enabling
				users to volunteer and seek out their altruistic and philanthropic
				tendencies, you can achieve the goals of positive computing that
				were previously established and contribute to a better future for
				society.</p>
		</div>
	</div>


	<div class="row">
		<div class="large-12 columns">
			<a href="${pageContext.request.contextPath}/"
				class="small button radius">Back</a>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<h5>References</h5>
			<span>Calvo, R. A., & Peters, D. (2012). Positive Computing: Technology for a Wiser World. Interactions , 19 (1072-5520), 28-31.</span>
			<span>Post, S. G. (2007). Altruism and Health: Perspectives from Empirical Research. New York: Oxford University Press.</span>
			<span>Post, S. G. (2008). Happiness, Health and Altruism. International Encyclopedia of Public Health . Elsevier Science & Technology.</span>
		</div>
	</div>
	<br />
	<br />

</body>
</html>

