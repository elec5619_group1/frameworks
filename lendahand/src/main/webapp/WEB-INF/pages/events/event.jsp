<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || ${pageContext.request.userPrincipal.name}</title>


<link href='<c:url value="/resources/css/foundation.css" />'
	rel="stylesheet">
<link href="<c:url value="/resources/css/locations.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/events.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/stats.css" />"
rel="stylesheet">
<link href='<c:url value="/resources/js/foundation.min.js" />'>
<link href='<c:url value="/resources/js/jquery.js" />'>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgexu0xK6ikbhRlkNLeIwdT1b4jiQ7Je0">
	
</script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	var username = "<c:out value="${pageContext.request.userPrincipal.name}" />";
	var model = []
	var event = [];
	console.log("<c:out value="${event.lat}" />");
	event.lat = "<c:out value="${event.lat}" />";
	event.lng = "<c:out value="${event.lng}" />";
	event.name = "<c:out value="${event.name}" />";
	event.createdBy = "<c:out value="${event.createdBy.username}"/>";
	model.push(event);
	console.log(event.lat);
</script>
<script src="../../resources/js/eventMap.js"></script>
<link href="<c:url value="/resources/js/eventMap.js" />">
</head>

<body>
	<!-- Top navigation bar -->
	<jsp:include page="../topNav.jsp" />



	<div class='hostbox-container'>
		<div class='hostbox'>
			<h3>Host: ${event.createdBy.username}</h3>
			<!-- User Profile Picture -->
			<c:choose>
				<c:when
					test="${event.createdBy.getProfileLink() == null || event.createdBy.getProfileLink() == ''}">
					<img src="http://placehold.it/200x150&text=No+Profile+Picture+Set">
				</c:when>
				<c:otherwise>
					<img src="http://${event.createdBy.getProfileLink()}"
						style="width: 200px; height: 150px">
				</c:otherwise>
			</c:choose>
			
			<div class="smallcontainer">
				<div class="icon">
					<img src="<c:url value="/resources/img/eternal_glyph.png"/>"
						class="symbol" />
				</div>
	
				<div class="score">
					<c:out value="${score}" />
				</div>
			</div>

			<div class="smallcontainer">
				<div class="icon">
					<img src="<c:url value="/resources/img/transient_glyph.png"/>"
						class="symbol" />
				</div>
				<div class="score">
					<c:out value="${scoreRecent}"/>
				</div>
			</div>
		</div>
	</div>

	<div class='contentbox'>
		<h1>${event.name}</h1>
		<p>${event.desc}</p>

		<div class="float-canvas">
			<div id="map-canvas"></div>
		</div>

		<br>

		<div class='column'>
			<div class='userlist'>
				<h3>Users Attending</h3>
				<table>
					<c:forEach items="${event.pledgedUsers}" var="user">
						<tr>
							<td>${user.getUsername()}</td>
						</tr>
					</c:forEach>
				</table>
			</div>

			<div class='share'>
				<form action="<c:url value='../../social/post/event'/>" method="POST">
					<input name="message" type="text" placeholder="Tell your friends something about this cause...">
					<input name="submit" type="submit" class="button">
					<input type=hidden name="id" value="<c:out value="${event.id}"/>" >
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</form>
			</div>

			<br>

			<div class='action-button'>
				<c:choose>
					<c:when
						test="${event.createdBy.getUsername()==pageContext.request.userPrincipal.name}">
						<c:if test="${event.awardingFinished==false}">
							<a class='butt'
								href="${pageContext.request.contextPath}/events/${event.id}/award">Give
								Karma </a>
							<br>
							<br>
							<a class='buttonLeave'
								href="${pageContext.request.contextPath}/events/delete/${event.id}">Delete
								Event</a>
						</c:if>
					</c:when>

					<c:otherwise>

						<c:set var="contains" value="false" />
						<c:forEach var="item" items="${event.pledgedUsers}">
							<c:if
								test="${item.getUsername() eq pageContext.request.userPrincipal.name}">
								<c:set var="contains" value="true" />
							</c:if>
						</c:forEach>

						<c:if test="${event.awardingFinished==false}">
							<c:choose>
								<c:when test="${contains==true}">
									<!-- <a class='buttonLeave'
										href="${pageContext.request.contextPath}/events/leave/${event.id}">Leave</a> -->
								</c:when>

								<c:otherwise>
									<a class='butt'
										href="${pageContext.request.contextPath}/events/join/${event.id}">Join</a>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:otherwise>
				</c:choose>
			</div>

		</div>
	</div>
</body>
</html>