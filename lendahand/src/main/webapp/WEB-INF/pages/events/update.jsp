<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
<title>LendAHand || Create Event</title>

<link href="<c:url value="/resources/css/foundation.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/foundation.min.css" />"
	rel="stylesheet">
</head>
<body onload='document.newEventForm.name.focus();'>


	<div align="center">


		<h1>Let's Start Giving Back, Have Some Fun...</h1>
		<h1>
			<strong><i>And Feel Great.</i></strong>
		</h1>
		<br>
	</div>
	<div align="center"
		style="margin-left: auto; margin-right: auto; width: 50%;">
		<h3>Create New Event!</h3>



		<form action="<c:url value='events/update/{eventId}/process'" method="post" commandname="updateEvent" id="updateForm">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
			<table>
				<tr>
					<td>Event name:</td>
					<td><input path="name" type='text' name='name'></td>
				</tr>
				<tr>
					<td>Location:</td>
					<td><input  path="loc" type='text' name='location' /></td>
				</tr>
				<tr>
					<td>Description:</td>
					<td><input  path="description" type='text' name='desc' /></td>
				</tr>
			</table>
		</form>
	</div>

</body>
</html>