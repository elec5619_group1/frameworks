<!DOCTYPE html>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>LendAHand || ${pageContext.request.userPrincipal.name}</title>


<link href='<c:url value="/resources/css/foundation.css" />'
	rel="stylesheet">
<link href='<c:url value="/resources/js/foundation.min.js" />'>
<link href="<c:url value="/resources/css/events.css" />"	rel="stylesheet">
<link href='<c:url value="/resources/js/jquery.js" />'>
</head>

<body>
	<!-- Top navigation bar -->
	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}/">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<ul class="right">
				<li class="active"><sec:authorize access="hasRole('ROLE_USER')">
						<c:url value="/logout" var="logoutUrl" />
						<form action="${logoutUrl}" method="post" id="logoutForm">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</form>
						<script>
							function formSubmit() {
								document.getElementById("logoutForm").submit();
							}
						</script>



						<a href="javascript:formSubmit()">Log Off</a>
					</sec:authorize></li>


			</ul>

			<!-- Left Nav Section -->
			<ul class="left">
				<li><a href=${pageContext.request.contextPath}/profile>Profile</a></li>
				<li><a href=${pageContext.request.contextPath}/events/eventlist>Causes</a></li>
				<li><a href=${pageContext.request.contextPath}/stats>Stats</a></li>
				<li><a href=${pageContext.request.contextPath}/connect/facebook>Social</a></li>
				<li><a href=${pageContext.request.contextPath}/map>Map</a></li>
			</ul>
		</section>
	</nav>

	<table class="contentlist">
		<tr>
			<th>Name</th>
			<th>Description</th>
			<th>Location</th>
			<th>Max Number of Volunteers</th>
			<th>Date</th>
			<th>Host</th>
			<th>Action</th>
		</tr>
		<c:forEach items="${eventList}" var="event">
			<tr>
				<td><a href="${pageContext.request.contextPath}/events/event/${event.id}">${event.name}</a></td>
				<td>${event.desc}</td>
				<td>${event.loc}</td>
				<td>${event.maxVolunteers}</td>
				<td>${event.startDate}</td>
				<td>${event.createdBy.getUsername()}</td>
				<td><c:choose>
						<c:when
							test="${event.createdBy.getUsername()==pageContext.request.userPrincipal.name}">
							<c:if test="${event.awardingFinished==false}">
									<a href="${pageContext.request.contextPath}/events/${event.id}/award">Give Karma</a>
									<br>
									<a href="${pageContext.request.contextPath}/events/delete/${event.id}">Delete</a>
							</c:if>	
						</c:when>

						<c:otherwise>
						
							<c:set var="contains" value="false" />
							<c:forEach var="item" items="${event.pledgedUsers}">
								<c:if test="${item.getUsername() eq pageContext.request.userPrincipal.name}">
									<c:set var="contains" value="true" />
								</c:if>
							</c:forEach>
							
							<c:if test="${event.awardingFinished==false}">
								<c:choose>
									<c:when test="${contains==true}">
										<!--<a href="${pageContext.request.contextPath}/events/leave/${event.id}">Leave</a> -->
									</c:when>
									
									<c:otherwise>
										<a href="${pageContext.request.contextPath}/events/join/${event.id}">Join</a>
									</c:otherwise>
								</c:choose>
							</c:if>
						</c:otherwise>
					</c:choose></td>
			</tr>
		</c:forEach>
	</table>

	<br>

	<a class='centre-butt' href="${pageContext.request.contextPath}/map">List
		A New Cause</a>

</body>
</html>