	<nav class="top-bar" data-topbar role="navigation">
		<ul class="title-area">
			<li class="name">
				<h1>
					<a href="${pageContext.request.contextPath}">LendAHand</a>
				</h1>
			</li>
			<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<!-- Right Nav Section | Includes log out button-->
			<ul class="right">
				<li class="active"><sec:authorize access="hasRole('ROLE_USER')">
						<c:url value="/logout" var="logoutUrl" />
						<form action="${logoutUrl}" method="post" id="logoutForm">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</form>
						<script>
							function formSubmit() {
								document.getElementById("logoutForm").submit();
							}
						</script>



						<a href="javascript:formSubmit()">Log Off</a>
					</sec:authorize></li>
			</ul>

			<!-- Left Nav Section -->
			<ul class="left">
				<li><a href=${pageContext.request.contextPath}/profile />Profile</a></li>
				<li><a href=${pageContext.request.contextPath}/events/eventlist>Causes</a></li>
				<li><a href=${pageContext.request.contextPath}/stats>Stats</a></li>
				<li><a href=${pageContext.request.contextPath}/connect/facebook>Social</a></li>
				<li><a href=${pageContext.request.contextPath}/map>Map</a></li>
			</ul>
		</section>
	</nav>