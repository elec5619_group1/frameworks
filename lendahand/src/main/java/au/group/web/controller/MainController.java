package au.group.web.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class MainController {

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView landingPage() {

		ModelAndView model = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.setView(new RedirectView("/profile", true, true, true));
			return model;
		}

		model.setViewName("landing");

		return model;
	}
	
	
	@RequestMapping(value = { "/about" }, method = RequestMethod.GET)
	public ModelAndView aboutPage() {

		ModelAndView model = new ModelAndView();

		model.setViewName("aboutUs");

		return model;
	}
	
	@RequestMapping(value = { "/whyvolunteer" }, method = RequestMethod.GET)
	public ModelAndView whyPage() {

		ModelAndView model = new ModelAndView();

		model.setViewName("whyVolunteer");

		return model;
	}
	
	
}