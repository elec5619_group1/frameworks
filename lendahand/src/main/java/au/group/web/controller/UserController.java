package au.group.web.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import au.group.events.model.Event;
import au.group.events.service.EventService;
import au.group.locations.service.MyLocationsService;
import au.group.social.service.SocialService;
import au.group.stats.service.StatsService;
import au.group.tools.DateComparer;
import au.group.users.model.User;
import au.group.users.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private StatsService statsService;

	@Autowired
	private EventService eventService;

	@Autowired
	private MyLocationsService locationService;

	@Autowired
	private SocialService socialServ;

	@RequestMapping(value = { "/register" }, method = RequestMethod.GET)
	public ModelAndView registerPage(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.setViewName("users/register");

		return model;
	}

	@RequestMapping(value = { "/register" }, method = RequestMethod.POST)
	public ModelAndView registerProcess(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		List<String> errors = new ArrayList<String>();
		/** Now we check errors */
		if (request.getParameter("username").toString() == "") {
			errors.add("No Username has been input.");
		}
		if (request.getParameter("password").toString() == "") {
			errors.add("No Password has been input.");
		}
		if (request.getParameter("email").toString() == "") {
			errors.add("No email has been input.");
		}

		if (!(request.getParameter("password").toString().equals(request
				.getParameter("passwordConf").toString()))) {
			errors.add("Password Confirmation does not match.");
		}
		if (userService.getUser(request.getParameter("username")) != null) {
			errors.add("Username already exists. Try another!");
		}
		if (userService.getUserByEmail(request.getParameter("email")) != null) {
			errors.add("Email already exists. Maybe you already have an account.");
		}
		/** Okay that should be enough error checks for registration. */

		/** If there are no errors, add the user and move to login page! */
		if (errors.isEmpty()) {
			String usname = request.getParameter("username").toString();
			String pword = request.getParameter("password").toString();
			String fName = request.getParameter("fName").toString();
			String lName = request.getParameter("lName").toString();
			String email = request.getParameter("email").toString();
			String location = request.getParameter("location").toString();

			userService
					.createUser(usname, pword, fName, lName, email, location);

			model.setView(new RedirectView("/login", true, true, true));

			return model;
		} else {
			/** If there are errors, notify user of errors. */
			model.addObject("errors", errors);
			model.setViewName("users/register");

			return model;
		}

	}

	@RequestMapping(value = { "/profile" }, method = RequestMethod.GET)
	public ModelAndView userProfilePage() {
		// Basic Profile
		ModelAndView model = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		User user = userService.getUser(auth.getName());
		model.addObject("user", user);
		model.addObject("score", statsService.getTotalKarma(user.getStat()));
		model.addObject("scoreRecent",
				statsService.getRecentKarma(user.getStat()));

		/* Pull out participated events */
		List<Event> events = new ArrayList<Event>();
		events = locationService.getParticipatedEvents(user);

		/* Build the map and add events to it */
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("eventsList", events);
		model.addAllObjects(modelMap);

		List<Event> ownEvents = eventService.getOwnEvents(user);
		List<Event> allEvents = eventService.getUpcomingEvents();
		List<Event> pledgedEvents = new ArrayList<Event>();
		for (Event e : allEvents) {
			for (User u : e.getPledgedUsers()) {
				if (u.getUsername().toString()
						.compareTo(user.getUsername().toString()) == 0) {
					System.out.println(u.getUsername() + " Matches "
							+ user.getUsername());
					pledgedEvents.add(e);
				}
			}
		}
		/*
		 * Order Own Events and restrict printing up to 3 events to the summary
		 * page
		 */
		Collections.sort(ownEvents, new DateComparer());
		if (ownEvents.size() <= 3) {
			model.addObject("ownEvents", ownEvents);
		} else {
			List<Event> topEvents = new ArrayList<Event>();
			topEvents.add(ownEvents.get(0));
			topEvents.add(ownEvents.get(1));
			topEvents.add(ownEvents.get(2));
			model.addObject("ownEvents", topEvents);
		}

		/* Do same for Pledged Events */
		Collections.sort(pledgedEvents, new DateComparer());
		if (pledgedEvents.size() <= 3) {
			model.addObject("joinedEvents", pledgedEvents);
		} else {
			List<Event> topJoinEvents = new ArrayList<Event>();
			topJoinEvents.add(pledgedEvents.get(0));
			topJoinEvents.add(pledgedEvents.get(1));
			topJoinEvents.add(pledgedEvents.get(2));
			model.addObject("joinedEvents", topJoinEvents);
		}

		/* Facebook module */

		//
		// Facebook facebook = socialServ.getFbInstance();
		//
		// List<FacebookProfile> friends =
		// facebook.friendOperations().getFriendProfiles();
		// model.addObject(friends);
		//
		model.setViewName("users/profile");
		return model;
	}

	@RequestMapping(value = { "/profile/edit" }, method = RequestMethod.GET)
	public ModelAndView editProfilePage() {
		ModelAndView model = new ModelAndView();

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		User user = userService.getUser(auth.getName());
		if (user.getDesc() == null) {
			user.setDesc("");
		}

		model.addObject("user", user);

		model.setViewName("users/editProfile");
		return model;
	}

	@RequestMapping(value = { "/profile/edit" }, method = RequestMethod.POST)
	public String editProcess(HttpServletRequest request) {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		User user = userService.getUser(auth.getName());

		String email = request.getParameter("email").toString();
		String location = request.getParameter("location").toString();
		String desc = request.getParameter("description").toString();
		String picLink = request.getParameter("profilePic").toString();

		userService.editUser(user, email, location, desc, picLink);

		return "redirect:./";

	}

	@RequestMapping(value = { "/user/{username}" }, method = RequestMethod.GET)
	public ModelAndView externalUserProfile(
			@PathVariable(value = "username") String username) {
		ModelAndView model = new ModelAndView();
		User user = userService.getUser(username);
		if (user == null) {
			model.setViewName("users/noProfile");
			return model;
		}

		/* Pull out participated events */
		List<Event> events = new ArrayList<Event>();
		events = locationService.getParticipatedEvents(user);

		/* Build the map and add events to it */
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("eventsList", events);
		model.addAllObjects(modelMap);

		List<Event> ownEvents = eventService.getOwnEvents(user);
		List<Event> allEvents = eventService.getUpcomingEvents();
		List<Event> pledgedEvents = new ArrayList<Event>();
		for (Event e : allEvents) {
			for (User u : e.getPledgedUsers()) {
				if (u.getUsername().toString()
						.compareTo(user.getUsername().toString()) == 0) {
					System.out.println(u.getUsername() + " Matches "
							+ user.getUsername());
					pledgedEvents.add(e);
				}
			}
		}
		/*
		 * Order Own Events and restrict printing up to 3 events to the summary
		 * page
		 */
		Collections.sort(ownEvents, new DateComparer());
		if (ownEvents.size() <= 3) {
			model.addObject("ownEvents", ownEvents);
		} else {
			List<Event> topEvents = new ArrayList<Event>();
			topEvents.add(ownEvents.get(0));
			topEvents.add(ownEvents.get(1));
			topEvents.add(ownEvents.get(2));
			model.addObject("ownEvents", topEvents);
		}

		/* Do same for Pledged Events */
		Collections.sort(pledgedEvents, new DateComparer());
		if (pledgedEvents.size() <= 3) {
			model.addObject("joinedEvents", pledgedEvents);
		} else {
			List<Event> topJoinEvents = new ArrayList<Event>();
			topJoinEvents.add(pledgedEvents.get(0));
			topJoinEvents.add(pledgedEvents.get(1));
			topJoinEvents.add(pledgedEvents.get(2));
			model.addObject("joinedEvents", topJoinEvents);
		}

		model.addObject("user", user);

		model.addObject("score", statsService.getTotalKarma(user.getStat()));
		model.addObject("scoreRecent",
				statsService.getRecentKarma(user.getStat()));
		model.setViewName("users/extProfile");
		return model;

	}

}
