package au.group.web.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.group.events.model.Event;
import au.group.events.service.EventService;
import au.group.locations.service.MyLocationsService;
import au.group.users.model.User;
import au.group.users.service.UserService;

@Controller
public class LocationController {

	@Autowired
	private MyLocationsService locationService;
	@Autowired
	private EventService eventService;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = { "/map" }, method = RequestMethod.GET)
	public ModelAndView locationPage() {

		ModelAndView model = new ModelAndView();
		model.setViewName("locations/map");
		//currently gets all the events in the database that haven't expired
		//and adds them to the model
		List<Event> events = new ArrayList<Event>();
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		//events = locationService.getAvailableEvents(userService.getUser(auth.getName()));
		events = locationService.getUpcomingEvents();
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("eventsList", events);
		model.addAllObjects(modelMap);
		return model;
	}
	
	// Currently, POST does not work in the application. For now we should use .GET...
	@RequestMapping(value = { "/map/create" }, method = RequestMethod.GET)
	public String createEventMap(HttpServletRequest request) {


		String lat = request.getParameter("lat").toString();
		String lng = request.getParameter("lng").toString();
		String startString = request.getParameter("start").toString();
		String desc = request.getParameter("desc").toString();
		String name = request.getParameter("name").toString();
		String createdByName = request.getParameter("createdBy").toString();
		String location = request.getParameter("location").toString();
		User createdBy = userService.getUser(createdByName);
		String maxV = request.getParameter("maxV").toString();
		String endString =  request.getParameter("end").toString();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = new Date();
		Date endDate = new Date();

		try {
			startDate = sdf.parse(startString);
			endDate = sdf.parse(endString);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		locationService.createEvent(lat, lng, startDate, desc, name, createdBy, maxV, location, endDate);

		return "redirect:../events/eventlist";

	}
	
	@RequestMapping(value={"/map/join"}, method=RequestMethod.GET)
	public String joinEvent(HttpServletRequest request){
		String id = request.getParameter("id");
		int eventId = Integer.parseInt(id);
		String pledgerName = request.getParameter("name");
		User pledger = userService.getUser(pledgerName);
		locationService.joinEvent(eventId, pledger);
		return "redirect:../map";
	}
	
	@RequestMapping(value={"/map/leave"}, method=RequestMethod.GET)
	public String leaveEvent(HttpServletRequest request){
		String id = request.getParameter("id");
		int eventId = Integer.parseInt(id);
		String pledgerName = request.getParameter("name");
		User pledger = userService.getUser(pledgerName);
		eventService.leaveEvent(eventId, pledger);
		return "redirect:../map";
	}
	
	@RequestMapping(value={"/map/deleteEvent"}, method=RequestMethod.GET)
	public String deleteeEvent(HttpServletRequest request){
		String id = request.getParameter("id");
		int eventId = Integer.parseInt(id);
		eventService.deleteEvent(eventId);
		return "redirect:../map";
	}
}
