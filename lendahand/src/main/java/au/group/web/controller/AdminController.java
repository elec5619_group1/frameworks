package au.group.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.group.users.model.User;
import au.group.users.service.UserService;

@Controller
public class AdminController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();

		List<User> users = userService.getUsers();

		model.addObject("users", users);

		model.setViewName("admin");

		return model;

	}

	@RequestMapping(value = { "/admin/togCharity" }, method = RequestMethod.POST)
	public String toggleCharity(HttpServletRequest request) {
		User user = userService.getUser(request.getParameter("username")
				.toString());
		if (user == null) {
			return "redirect:./";
		}

		if (user.isCharity()) {
			userService.removeCharity(user);
		} else {
			userService.addCharity(user);
		}

		return "redirect:./";

	}
	
	@RequestMapping(value = { "/admin/togEnabled" }, method = RequestMethod.POST)
	public String toggleEnabled(HttpServletRequest request) {
		User user = userService.getUser(request.getParameter("username")
				.toString());
		if (user == null) {
			return "redirect:./";
		}

		if (user.isEnabled()) {
			userService.removeEnablement(user);
		} else {
			userService.addEnablement(user);
		}

		return "redirect:./";

	}

}
