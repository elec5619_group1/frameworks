package au.group.web.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookLink;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.group.events.service.EventService;
import au.group.social.service.SocialService;

@Controller
public class SocialController {

	//@Autowired
	private Facebook fb; /*final*/
		
	@Autowired
	private EventService eventServ;
	
	@Autowired
	private SocialService socialServ;
	
	@Inject
	public SocialController(Facebook facebook)
	{
		fb = facebook;
		String token = "CAANaJgomKg4BALwc5YtKDClhngeEjdoJh39i1jKCE675NL8mteECtFJn0iGCZBgg1Cs3kDmguXfIQjPqutRIuzbZAnJAVg2wmgprgWbunA2wt2ZArqnw50pnONRhKvE4WbBUQULW3FAImM6gx1mP5CHu0KgdZBtpRpdnFbconXenHAPhzZCZBaPrIa066J2YCR1C2h4soTaCxYwkFKKZAGI";
		fb = new FacebookTemplate(token);
	}
	
	@RequestMapping(value= {"/connect/facebook"}, method = RequestMethod.GET)
	public ModelAndView socialPage() { //@RequestParam(value="code",required=false) String code
		
//		Authentication auth = SecurityContextHolder.getContext()
//				.getAuthentication();
//		String uname = auth.getName();
//		String token = socialServ.getToken(uname);

		if(!fb.isAuthorized())
		{
			ModelAndView connect = new ModelAndView();
			connect.setViewName("/connect/facebookConnect");
			return connect;
		}
		// Basic Profile
		ModelAndView model = new ModelAndView();
		PagedList<Post> feed = fb.feedOperations().getHomeFeed();
		FacebookProfile profileData = fb.userOperations().getUserProfile();
		model.addObject("feed", feed);
		model.addObject("data", profileData);
		model.setViewName("Social/connectFb");
		return model;
	}
	
	@RequestMapping(value= {"/social/post"}, method = RequestMethod.POST)
	public ModelAndView postStat(HttpServletRequest request){
		String message = request.getParameter("message");
		FacebookLink link = new FacebookLink(
				"localhost:8080/SpringSecurity", 
				"LendAHand", 
				"Helping people one hand at a time", 
				"Find Causes around your location that need your help! If you need help with something yourself, but it online and let the world know you need some people to lend a hand.");
		fb.feedOperations().postLink(message, link);
		ModelAndView model = new ModelAndView("redirect:../connect/facebook");
		return model;
	}
	
	@RequestMapping(value= {"/social/post/event"}, method = RequestMethod.GET)
	public ModelAndView shareEvent(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("Social/events");
		mv.addObject("eventList", eventServ.listOfEvents());
		return mv;
	}
	
	@RequestMapping(value= {"/social/post/event"}, method = RequestMethod.POST)
	public ModelAndView postEvent(HttpServletRequest request){
		if(!fb.isAuthorized())
		{
			ModelAndView connect = new ModelAndView();
			connect.setViewName("/connect/facebookConnect");
			return connect;
		}
		String url = "localhost:8080/SpringSecurity/events/event/"+ request.getParameter("id");
		FacebookLink link = new FacebookLink(url, "LendAHand","Helping people one hand at a time", 
				"Find Causes around your location that need your help! If you need help with something yourself, but it online and let the world know you need some people to lend a hand.");
		String message = request.getParameter("message");
		fb.feedOperations().postLink(message, link);
		ModelAndView model = new ModelAndView();
		model.setViewName("events/eventlist");
		return model;
	}

}
