package au.group.web.controller;

import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.group.events.model.Event;
import au.group.events.service.EventService;
import au.group.locations.service.MyLocationsService;
import au.group.stats.model.Stat;
import au.group.stats.service.StatsService;
import au.group.tools.DateComparer;
import au.group.users.model.User;
import au.group.users.service.UserService;

@Controller
@RequestMapping(value = "/events/**")
public class EventController {

	@Autowired
	private EventService eventService;
	@Autowired
	private UserService userService;
	@Autowired
	private MyLocationsService locationService;
	@Autowired
	private StatsService statsService;

	// CREATE Event
	@RequestMapping(value = "/createevent", method = RequestMethod.GET)
	public ModelAndView createEventPage(HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.setViewName("events/createevent");

		return model;

	}

	// READ List of Events
	@RequestMapping(value = "/eventlist", method = RequestMethod.GET)
	public String eventListPage(Model model) {
		List<Event> events = eventService.listOfEvents();
		Collections.sort(events, new DateComparer());
		model.addAttribute("eventList", events);

		return "/events/eventlist";
	}

	// UPDATE Event Details
	@RequestMapping(value = "/update/{eventId}", method = RequestMethod.GET)
	public ModelAndView updateEvent(@PathVariable("eventId") int eventId,HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		model.setViewName("events/update/{eventId}");
		Event updatingEvent = eventService.findEventById(eventId);
		model.addObject("updateEvent", updatingEvent);

		return model;

	}

	// UPDATE Event Details
	@RequestMapping(value = "/update/{eventId}/process", method = RequestMethod.POST)
	public String updateEventProcess(@PathVariable("eventId") int eventId,
			HttpServletRequest request) {

		String name = request.getParameter("name").toString();
		String location = request.getParameter("location").toString();
		String desc = request.getParameter("desc").toString();

		eventService.updateEvent(eventId, name, location, desc);

		return "redirect:../events/eventlist";
	}

	// DELETE Event
	@RequestMapping(value = "/delete/{eventId}")
	public String deleteEvent(@PathVariable("eventId") int eventId) {

		eventService.deleteEvent(eventId);

		return "redirect:../events/eventlist";
	}

	// JOIN Event
	@RequestMapping(value = "/join/{eventId}")
	public String joinEvent(@PathVariable("eventId") int eventId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User pledger = userService.getUser(auth.getName());
		locationService.joinEvent(eventId, pledger);
		String url = "redirect:../events/event/".concat(Integer.toString(eventId));

		return url;
	}
	
	// LEAVE Event
	@RequestMapping(value = "/leave/{eventId}")
	public String leaveEvent(@PathVariable("eventId") int eventId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User pledger = userService.getUser(auth.getName());
		System.out.println("FLAG");
		eventService.leaveEvent(eventId, pledger);

		return "redirect:../events/eventlist";
	}
	
	// Show Event
	@RequestMapping(value = "/event/{eventId}", method = RequestMethod.GET)
	public String displayEvent(@PathVariable("eventId") int eventId, Model model) {
		model.addAttribute("event", eventService.findEventById(eventId));
		
		User host = eventService.findEventById(eventId).getCreatedBy();
		
		Stat stat = statsService.findByUsername(host.getUsername());
		
		if (stat != null)
		{
			model.addAttribute("host", host);
			model.addAttribute("score", statsService.getTotalKarma(stat));
			model.addAttribute("scoreRecent", statsService.getRecentKarma(stat));
		}
		
		return "/events/event";
	}

}
