package au.group.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;

import au.group.events.model.Event;
import au.group.events.service.EventService;
import au.group.stats.model.Badge;
import au.group.stats.model.Stat;
import au.group.stats.service.StatsService;
import au.group.users.service.UserService;

@Controller
@Transactional
public class StatController {
	
	@Autowired
	private StatsService statsService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private EventService eventService;
	
	@RequestMapping(value = { "/{username}/stats" }, method = RequestMethod.GET)
	public String statsPage(@PathVariable("username") String username, Model model) {
		
		Stat stat = statsService.findByUsername(username);
		
		if (stat != null)
		{
			model.addAttribute("user", stat.getUser());
			model.addAttribute("score", statsService.getTotalKarma(stat));
			model.addAttribute("scoreRecent", statsService.getRecentKarma(stat));
			return "stats/show";
		}
		
		return "404";
	}
	
	@RequestMapping(value = { "/{username}/stats/display" }, method = RequestMethod.GET)
	public String statsDisplayPage(@PathVariable("username") String username, Model model) {
		
		Stat stat = statsService.findByUsername(username);
		
		if (stat != null)
		{
			model.addAttribute("user", stat.getUser());
			model.addAttribute("score", statsService.getTotalKarma(stat));
			model.addAttribute("scoreRecent", statsService.getRecentKarma(stat));
			return "stats/stats_display";
		}
		
		return "404";
	}
	
	@RequestMapping(value = { "/stats" }, method = RequestMethod.GET)
	public String statsPage(Model model) {
		
		au.group.users.model.User currentUser = userService.getLoggedInUser();
		if (currentUser == null)
			return "404";
		
		Stat stat = statsService.findByUsername(currentUser.getUsername());

		model.addAttribute("user", stat.getUser());
		model.addAttribute("score", statsService.getTotalKarma(stat));
		model.addAttribute("scoreRecent", statsService.getRecentKarma(stat));
		return "stats/show";
	}
	
	@RequestMapping(value = { "/events/{eventid}/award" }, method = RequestMethod.GET)
	@Transactional
	public String awardKarmaPage(@PathVariable("eventid") int eventId, Model model) {
	
		Event event = eventService.findEventById(eventId);
		
		if (event == null || event.isAwardingFinished())
			return "404";
		
		au.group.users.model.User currentUser = userService.getLoggedInUser();
		
		if (currentUser == null)
			return "403";
		
		String a = event.getCreatedBy().getUsername();
		String b = currentUser.getUsername();

		
		if (!a.equals(b))
			return "403";
		
		model.addAttribute("cause", event);
		model.addAttribute("stat", statsService.findByUsername(currentUser.getUsername()));
		
		return "stats/award_karma";
	}
	
	@RequestMapping(value = { "/events/{eventid}/award" }, method = RequestMethod.POST)
	@Transactional
	public String awardKarmaForm(@PathVariable("eventid") int eventId, Model model, HttpServletRequest request) {
	
		Event event = eventService.findEventById(eventId);
		
		if (event == null || event.isAwardingFinished())
			return "404";
		
		au.group.users.model.User currentUser = userService.getLoggedInUser();
		String a = event.getCreatedBy().getUsername();
		String b = currentUser.getUsername();
		
		if (!a.equals(b))
			return "403";
		
		for (au.group.users.model.User u : event.getPledgedUsers())
		{
			String status = request.getParameter(u.getUsername());
			if (status != null)
			{
				if (status.equals("on"))
				{
					statsService.awardKarma(u, event);
				}
			}
			
			String badgeId=request.getParameter("badge-selection");
			Badge selectedBadge=null;
			List<Stat> badgeEarners = null;
			if (badgeId != null && badgeId !="")
			{
				selectedBadge=statsService.getBadgeById(Long.parseLong(badgeId));
				model.addAttribute("badge",selectedBadge);
				badgeEarners = new ArrayList<Stat>();
			}
			
			String badgeStatus = request.getParameter(u.getUsername()+"_badge");
			if (badgeStatus != null)
			{
				if (badgeStatus.equals("on"))
				{
					statsService.awardBadge(u, selectedBadge);
					badgeEarners.add(statsService.findByUsername(u.getUsername()));
				}
			}
			
			if (badgeEarners != null)
				model.addAttribute("badgeEarners", badgeEarners);
		}
		
		
		//block any more karma awarding
		eventService.finishAwarding(eventId);
		
		model.addAttribute("cause", event);
		
		return "stats/show_awarded_karma";
	}
	
	@RequestMapping(value = { "/badges" }, method = RequestMethod.GET)
	@Transactional
	public String listBadges(Model model) {
	
		au.group.users.model.User currentUser = userService.getLoggedInUser();
		
		if (currentUser == null)
			return "403";
		
		//if (!currentUser.isCharity())
			//return "405";
		
		
		
		model.addAttribute("stat", statsService.findByUsername(currentUser.getUsername()));
		
		return "stats/show_badges";
	}
	
	@RequestMapping(value = { "/badges/create" }, method = RequestMethod.GET)
	@Transactional
	public String createBadgePage(Model model) {
	
		au.group.users.model.User currentUser = userService.getLoggedInUser();
		
		if (currentUser == null)
			return "403";
		
		//if (!currentUser.isCharity())
		// "405";
		
		
		
		return "stats/create_badge";
	}
	
	@RequestMapping(value = { "/badges/create" }, method = RequestMethod.POST)
	@Transactional
	public String createBadgeForm(Model model, HttpServletRequest request) {
		
		au.group.users.model.User currentUser = userService.getLoggedInUser();
		
		if (currentUser == null)
			return "403";
		
		//if (!currentUser.isCharity())
			//return "405";
		
		statsService.createBadge(currentUser, request.getParameter("name"), request.getParameter("badgeSVG"));
		
		model.addAttribute("stat", statsService.findByUsername(currentUser.getUsername()));
		
		
		return "stats/show_badges";
	}
}
