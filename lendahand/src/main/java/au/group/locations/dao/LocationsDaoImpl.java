package au.group.locations.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.group.events.model.Event;
import au.group.users.model.User;


@Repository
public class LocationsDaoImpl implements LocationsDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Event> getAvailableByLocation(float topLat, float leftLong, float bottomLat, float rightLong) {
		List<Event> events = new ArrayList<Event>();
		events=sessionFactory.getCurrentSession().createQuery("FROM events WHERE lat>=? AND lat<=? AND lng>=? AND lng<=? AND endDate>=now() AND currVolunteers<maxVolunteers").setParameter(0, bottomLat).setParameter(1, topLat).setParameter(2,leftLong).setParameter(3, rightLong).list();
		return events;
		
	}
	/**
	 * returns all events that are available for the logged in user to join
	 * @param u - currently logged in user
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Event> getAvailableEvents() {
		List<Event> events = new ArrayList<Event>();
		System.out.println("SESSIONFACTORY="+sessionFactory);
		events=sessionFactory.getCurrentSession().createQuery("FROM Event WHERE endDate>=now()").list();
		//remove all of the events this user has already pledged to
		//Set<User> pledgers = new HashSet();
		
		return events;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Event> getPledgedEvents(User u){
		List<Event> events = new ArrayList<Event>();
		//System.out.println("SESSIONFACTORY="+sessionFactory);
		events=sessionFactory.getCurrentSession().createQuery("FROM Event WHERE endDate>=now()").list();
		return events;
	}

	
	@Override
	public Event createEvent(float latF, float lngF, Date start, String desc,
			String name, User createdBy, int maxVol, String location, Date end) {
		
		Event newEvent = new Event();
		newEvent.setLat(latF);
		newEvent.setLng(lngF);
		newEvent.setDateCreatedToNow();
		newEvent.setStartDate(start);
		newEvent.setDesc(desc);
		newEvent.setName(name);
		newEvent.setCreatedBy(createdBy);
		newEvent.setMaxVolunteers(maxVol);
		newEvent.setLoc(location);
		//newEvent.setCurrVolunteers(0);
		newEvent.setEndDate(end);
		
		sessionFactory.getCurrentSession().save(newEvent);
		return newEvent;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Event joinEvent(int id, User pledger){
		List<Event> e = new ArrayList<Event>();
		e = sessionFactory.getCurrentSession().createQuery("From Event WHERE id = ?").setParameter(0, (Integer)id).list();
		if(e.size()==1){
			Event event = e.get(0);
			event.addPledger(pledger);
			//event.setCurrVolunteers(event.getCurrVolunteers()+1);
			sessionFactory.getCurrentSession().merge(event);
			return event;
		}
		else if(e.size()==0){
			System.out.println("EVENT DOESNT EXIST!");
			return null;
		}
		else {
			System.out.println("More than one event!");
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Event> getFinishedEvents() {
		// TODO Auto-generated method stub
		List<Event> e = new ArrayList<Event>();
		e = sessionFactory.getCurrentSession().createQuery("From Event WHERE endDate<=now()").list();
		return e;
	}

}
