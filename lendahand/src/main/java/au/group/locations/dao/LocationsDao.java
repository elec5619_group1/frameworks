package au.group.locations.dao;

import java.util.Date;
import java.util.List;

import au.group.events.model.Event;
import au.group.users.model.User;



public interface LocationsDao {

	public List<Event> getAvailableByLocation(float topLat, float leftLong, float bottomLat, float rightLong);
	public List<Event> getAvailableEvents();
	public List<Event> getPledgedEvents(User u);
	Event createEvent(float latF, float lngF, Date start, String desc,
			String name, User createdBy, int maxVol, String location, Date end);
	Event joinEvent(int id, User pledger);
	List<Event> getFinishedEvents();
	
}
