package au.group.locations.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.group.events.model.Event;
import au.group.locations.dao.LocationsDaoImpl;
import au.group.users.model.User;

@Service("locationsService")
public class MyLocationsService {

	@Autowired
	private LocationsDaoImpl ldao;

	@Transactional
	public List<Event> getActiveEventsByLocation(float left, float right,
			float top, float bot) {
		List<Event> events = new ArrayList<Event>();
		events = ldao.getAvailableByLocation(top, left, bot, right);
		return events;

	}

	@Transactional
	public List<Event> getParticipatedEvents(User u) {
		List<Event> events = new ArrayList<Event>();
		events = ldao.getFinishedEvents();
		System.out.println("There are " + events.size()
				+ " finshed events in the db");
		List<Event> eventsToRemove = new ArrayList<Event>();
		boolean pledged = false;
		for (Event e : events) {

			System.out.println("event: " + e.getId() + " has "
					+ e.getPledgedUsers().size() + " pledged users.");
			System.out.println("looking for: " + u.getUsername() + " in event "
					+ e.getId());
			for (User user : e.getPledgedUsers()) {
				if (user.getUsername().equals(u.getUsername())) {
					System.out.println("pledged");
					pledged = true;
					break;
				}
			}
			if (!pledged) {
				eventsToRemove.add(e);
				// events.remove(e);
			} else {
				pledged = false;
			}
		}
		for (Event e : eventsToRemove) {
			events.remove(e);
		}
		System.out.println("Events participated in size = " + events.size());
		return events;

	}
	@Transactional
	public List<Event> getUpcomingEvents() {
		List<Event> events = new ArrayList<Event>();
		events = ldao.getAvailableEvents();
		return events;
	}

	@Transactional
	public List<Event> getAvailableEvents(User u) {
		List<Event> events = new ArrayList<Event>();
		events = ldao.getAvailableEvents();
		List<Event> eventsToRemove = new ArrayList<Event>();
		for (Event e : events) {

			System.out.println("event: " + e.getId() + " has "
					+ e.getPledgedUsers().size() + " pledged users.");
			System.out.println("looking for: " + u.getUsername() + " in event "
					+ e.getId());
			for (User user : e.getPledgedUsers()) {
				if (user.getUsername().equals(u.getUsername())||e.getPledgedUsers().size()>=e.getMaxVolunteers()) {
					//System.out.println("pledged");
					eventsToRemove.add(e);
					// events.remove(e);
					break;
				}
			}
		}
		for (Event e : eventsToRemove) {
			events.remove(e);
		}
		return events;
	}

	@Transactional
	public void createEvent(String lat, String lng, Date start, String desc,
			String name, User createdBy, String maxV, String location, Date end) {
		// TODO Auto-generated method stub
		int maxVol = Integer.parseInt(maxV);
		float latF = Float.parseFloat(lat);
		float lngF = Float.parseFloat(lng);
		ldao.createEvent(latF, lngF, start, desc, name, createdBy, maxVol, location, end);

	}

	@Transactional
	public void joinEvent(int eventId, User pledger) {
		// int id = Integer.parseInt(eventId);

		ldao.joinEvent(eventId, pledger);
	}

}
