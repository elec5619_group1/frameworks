package au.group.events.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.group.events.dao.EventDao;
import au.group.events.model.Event;
import au.group.users.model.User;

@Service
public class EventService {

	@Autowired
	private EventDao eventDao;
	
	@Transactional
	public void createEvent(String name, String desc, String loc,
			User createdBy, int maxVolunteers, Date startDate) {
		eventDao.createEvent(name, desc, loc, createdBy, maxVolunteers,
				startDate);
	}

	@Transactional
	public void deleteEvent(int eventId) {
		eventDao.deleteEvent(eventId);
	}

	@Transactional
	public void updateEvent(int eventId, String name, String location,
			String desc) {
		eventDao.updateEvent(eventId, name, location, desc);
	}

	@Transactional
	public void leaveEvent(int eventId, User pledger) {
		System.out.println("FLAG");
		eventDao.leaveEvent(eventId, pledger);
	}

	@Transactional
	public List<Event> listOfEvents() {
		return eventDao.listOfEvents();
	}

	@Transactional
	public Event findEventById(int id) {
		return eventDao.findEventById(id);
	}

	@Transactional
	public void finishAwarding(int eventId) {
		eventDao.finishEventAwarding(eventId);
	}

	@Transactional
	public List<Event> getOwnEvents(User user) {
		return eventDao.getOwnEvents(user);
	}
	
	@Transactional
	public List<Event> getUpcomingEvents() {
		return eventDao.getUpcomingEvents();
	}

}
