package au.group.events.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import au.group.stats.model.Karma;
import au.group.users.model.User;

@Entity
@Table(name = "events", catalog = "lendahand")
public class Event {

	private int id;
	private String name;
	private Date dateCreated;
	private Date startDate;
	private Date endDate;
	private String description;
	private String loc;
	private User createdBy;
	private int maxVolunteers;
	private float lat;
	private float lng;
	
	//private List<User> currVolunteers;
	private Set<User> pledgedUsers = new HashSet<User>();
	private Set<Karma> awardedUsers;
	private boolean isAwardingFinished;
	
	public Event(){
	}
	
	public Event(String name, String description, String loc, User createdBy, int maxVolunteers,Date startDate) {
		this.name = name;
		this.dateCreated = new Date();
		this.startDate = startDate;
		this.description = description;
		this.loc = loc;
		this.createdBy = createdBy;
		this.maxVolunteers = maxVolunteers;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id", unique = true)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="loc")
	@Type(type="string")
	public String getLoc() {
		return loc;
	}
	public void setLoc(String loc) {
		this.loc = loc;
	}
	
	@Column(name="dateCreated")
	@Type(type="date")
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public void setDateCreatedToNow(){
		this.dateCreated = new Date();
	}
	
	@Column(name="startDate")
	@Type(type="date")
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@Column(name="description")
	@Type(type="string")
	public String getDesc() {
		return description;
	}
	public void setDesc(String desc) {
		this.description = desc;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "username", nullable = false)
	public User getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name="maxVolunteers")
	@Type(type="integer")
	public int getMaxVolunteers() {
		return maxVolunteers;
	}
	public void setMaxVolunteers(int maxVolunteers) {
		this.maxVolunteers = maxVolunteers;
	}

	@Column(name="lat")
	@Type(type="float")
	public void setLat(float lat){
		this.lat = lat;
	}
	public float getLat(){
		return this.lat;
	}
	@Column(name="lng")
	@Type(type="float")
	public void setLng(float lng){
		this.lng = lng;
	}
	public float getLng(){
		return this.lng;
	}
	/*
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "username", nullable = true)
	public List<User> getCurrVolunteers() {
		return currVolunteers;
	*/

	/* DEANS ADDITIONS */
	@ManyToMany(cascade = {CascadeType.ALL}, fetch=FetchType.EAGER)
    @JoinTable(name="event_pledges", 
                joinColumns={@JoinColumn(name="event_id")}, 
                inverseJoinColumns={@JoinColumn(name="user_id")})
	public Set<User> getPledgedUsers() {
		return pledgedUsers;
	}

	public void setPledgedUsers(Set<User> pledgedUsers) {
		this.pledgedUsers = pledgedUsers;
	}
	
	
	public void addPledger(User pledger){
		this.pledgedUsers.add(pledger);
	}
	
	public void removePledger(User pledger){
		this.pledgedUsers.remove(pledger);
	}
	
	@OneToMany(mappedBy="event", fetch=FetchType.EAGER)
	public Set<Karma> getAwardedUsers() {
		return awardedUsers;
	}

	public void setAwardedUsers(Set<Karma> awardedUsers) {
		this.awardedUsers = awardedUsers;
	}
	
	@Column(name = "awarding_finished")
	@Type(type="boolean")
	public boolean isAwardingFinished() {
		return isAwardingFinished;
	}

	/**
	 * @return the endDate
	 */
	@Column(name="endDate")
	@Type(type="date")
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setAwardingFinished(boolean isAwardingFinished) {
		this.isAwardingFinished = isAwardingFinished;

	}
}
