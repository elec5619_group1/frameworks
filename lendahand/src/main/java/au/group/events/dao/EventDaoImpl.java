package au.group.events.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import au.group.events.model.Event;
import au.group.users.model.User;

@Repository
public class EventDaoImpl implements EventDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void createEvent(String name, String desc, String loc,
			User createdBy, int maxVolunteers, Date startDate) {

		Event newEvent = new Event(name, desc, loc, createdBy, maxVolunteers,
				startDate);
		sessionFactory.getCurrentSession().save(newEvent);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Event> listOfEvents() {
		return sessionFactory.openSession().createQuery("from Event").list();
	}

	@Override
	public void registerForEvent(int eventId, User newVolunteer) {
		// add user id to list of those attending
		Event currEvent = findEventById(eventId);
		// currEvent.addToCurrVolunteers(newVolunteer);

		sessionFactory.getCurrentSession().save(currEvent);
		// need to add anything to User object?
	}

	@SuppressWarnings("unchecked")
	@Override
	public Event findEventById(int eventId) {

		List<Event> events = new ArrayList<Event>();

		events = sessionFactory.openSession()
				.createQuery("from Event where id=?").setParameter(0, eventId)
				.list();

		if (events.size() > 0) {
			return events.get(0);
		} else {
			return null;
		}

	}

	@Override
	public void deleteEvent(int eventId) {
		Event deletionEvent = (Event) sessionFactory.getCurrentSession().load(
				Event.class, eventId);
		if (null != deletionEvent) {
			this.sessionFactory.getCurrentSession().delete(deletionEvent);
		}
	}

	@Override
	public void updateEvent(int eventId, String name, String location,
			String desc) {
		Event updateEvent = (Event) sessionFactory.getCurrentSession().load(
				Event.class, eventId);
		updateEvent.setName(name);
		updateEvent.setLoc(location);
		updateEvent.setDesc(desc);

		if (null != updateEvent) {
			this.sessionFactory.getCurrentSession().merge(updateEvent);
		}
	}

	// DEANS
	@Override
	public void finishEventAwarding(int eventId) {
		Event updateEvent = (Event) sessionFactory.getCurrentSession().load(
				Event.class, eventId);
		updateEvent.setAwardingFinished(true);

		if (null != updateEvent) {
			this.sessionFactory.getCurrentSession().update(updateEvent);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Event leaveEvent(int eventId, User pledger) {
		List<Event> e = new ArrayList<Event>();
		e = sessionFactory.getCurrentSession().createQuery("From Event WHERE id = ?").setParameter(0, (Integer)eventId).list();
		System.out.println("FLAG");
		if(e.size()==1){
			Event event = e.get(0);
			event.removePledger(pledger);
			sessionFactory.getCurrentSession().merge(event);
			return event;
		}
		else if(e.size()==0){
			System.out.println("EVENT DOESNT EXIST!");
			return null;
		}
		else {
			System.out.println("More than one event!");
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Event> getOwnEvents(User user) {

		List<Event> events = new ArrayList<Event>();

		// String uName = user.getUsername();

		events = sessionFactory.getCurrentSession()
				.createQuery("from Event where username=?")
				.setParameter(0, user).list();

		return events;
	}

	@SuppressWarnings("unchecked")
	public List<Event> getUpcomingEvents() {
		List<Event> events = new ArrayList<Event>();
		events = sessionFactory.getCurrentSession()
				.createQuery("FROM Event WHERE startDate>=now()").list();

		return events;
	}

}
