package au.group.events.dao;

import java.util.Date;
import java.util.List;

import au.group.events.model.Event;
import au.group.users.model.User;



public interface EventDao {

	public void createEvent(String name, String desc, String loc, User createdBy, int maxVolunteers,Date startDate);
	public void registerForEvent(int eventId, User newVolunteer);
	public List<Event> listOfEvents();
	public void deleteEvent(int eventId);
	public void updateEvent(int eventId, String name, String location, String desc);
	public Event findEventById(int eventId);
	public Event leaveEvent(int eventId, User pledger);
	
	//DEANS
	public void finishEventAwarding(int eventId);
	
	//KERRODS
	public List<Event> getOwnEvents(User user);
	public List<Event> getUpcomingEvents();
}
