package au.group.users.dao;

import java.util.List;

import au.group.users.model.User;

public interface UserDao {

	User findByUserName(String username);
	User findByEmail(String email);
	User createNewUser(String username, String password, String fName, String lName, String email, String location);
	User editUser(User user, String email, String location, String desc, String profilePic);
	List<User> getUsers();
	void addCharity(User user);
	void removeCharity(User user);
	void addEnablement(User user);
	void removeEnablement(User user);
}