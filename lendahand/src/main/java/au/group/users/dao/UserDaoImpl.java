package au.group.users.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import au.group.stats.model.Stat;
import au.group.users.model.User;
import au.group.users.model.UserRole;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public User findByUserName(String username) {

		List<User> users = new ArrayList<User>();

		users = sessionFactory.getCurrentSession()
				.createQuery("from User where username=?")
				.setParameter(0, username).list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	public User findByEmail(String email) {

		List<User> users = new ArrayList<User>();

		users = sessionFactory.getCurrentSession()
				.createQuery("from User where email=?").setParameter(0, email)
				.list();

		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}

	public User createNewUser(String username, String password, String fName,
			String lName, String email, String location) {

		User newUser = new User();
		newUser.setUsername(username);
		// encrypt password
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		String newPass = encoder.encode(password);
		newUser.setPassword(newPass);
		newUser.setFName(fName);
		newUser.setLName(lName);
		newUser.setEnabled(true);
		newUser.setEmail(email);
		newUser.setLocation(location);
		newUser.setCharity(false);

		UserRole newRole = new UserRole();

		newRole.setUser(newUser);
		newRole.setRole("ROLE_USER");

		Stat newStat = new Stat(newUser);
		newUser.setStat(newStat);

		sessionFactory.getCurrentSession().save(newUser);
		sessionFactory.getCurrentSession().save(newRole);
		sessionFactory.getCurrentSession().save(newStat);

		return newUser;
	}

	public User editUser(User user, String email, String location, String desc,
			String profilePic) {

		User pUser = user;

		/**
		 * I could compare the new result with the old result but it seems more
		 * efficient to just push them all even if they are the same
		 */
		pUser.setEmail(email);
		pUser.setLocation(location);
		pUser.setDesc(desc);
		pUser.setProfileLink(profilePic);

		sessionFactory.getCurrentSession().merge(pUser);

		return pUser;
	}


	@SuppressWarnings("unchecked")
	public List<User> getUsers() {

		List<User> users = new ArrayList<User>();

		users = sessionFactory.getCurrentSession().createQuery("from User")
				.list();

		return users;
	}

	public void addCharity(User user) {

		user.setCharity(true);
		sessionFactory.getCurrentSession().merge(user);

	}

	public void removeCharity(User user) {

		user.setCharity(false);
		sessionFactory.getCurrentSession().merge(user);
	}
	
	public void addEnablement(User user) {

		user.setEnabled(true);
		sessionFactory.getCurrentSession().merge(user);

	}

	public void removeEnablement(User user) {

		user.setEnabled(false);
		sessionFactory.getCurrentSession().merge(user);
	}

}