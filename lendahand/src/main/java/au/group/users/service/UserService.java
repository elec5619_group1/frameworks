package au.group.users.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.group.users.dao.UserDao;
import au.group.users.model.User;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	@Transactional
	public String createUser(String username, String password, String fName,
			String lName, String email, String location) {

		User res = userDao.findByUserName(username);

		if (res != null) {
			return "Taken";
		} else {
			userDao.createNewUser(username, password, fName, lName, email,
					location);
			return "Free";
		}

	}

	@Transactional
	public User editUser(User user, String email, String location, String desc,
			String profilePic) {

		userDao.editUser(user, email, location, desc, profilePic);

		return user;

	}

	/* The following is taken from MyUserDetailsService...kind of */
	@Transactional(readOnly = true)
	public User searchUserByUsername(final String username) {

		User usr = userDao.findByUserName(username);

		return usr;

	}

	/* Dean put this here */
	public User getLoggedInUser() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		User user = searchUserByUsername(auth.getName());

		return user;
	}

	@Transactional(readOnly = true)
	public User getUser(String username) {

		User user = userDao.findByUserName(username);

		return user;
	}

	@Transactional(readOnly = true)
	public User getUserByEmail(String email) {

		User user = userDao.findByEmail(email);

		return user;
	}

	@Transactional(readOnly = true)
	public List<User> getUsers() {

		List<User> users = new ArrayList<User>();

		users = userDao.getUsers();

		return users;
	}

	@Transactional
	public void addCharity(User user) {

		userDao.addCharity(user);

	}

	@Transactional
	public void removeCharity(User user) {

		userDao.removeCharity(user);

	}
	
	@Transactional
	public void addEnablement(User user) {

		userDao.addEnablement(user);

	}

	@Transactional
	public void removeEnablement(User user) {

		userDao.removeEnablement(user);

	}

}
