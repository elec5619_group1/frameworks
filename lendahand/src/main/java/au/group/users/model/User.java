package au.group.users.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import au.group.events.model.Event;
import au.group.stats.model.Badge;
import au.group.stats.model.Stat;

@Entity
@Table(name = "users", catalog = "lendahand")
public class User {

	private String username;
	private String password;
	private boolean enabled;
	private Set<UserRole> userRole = new HashSet<UserRole>(0);
	private String email;
	private String location;
	private String description;
	private String fName;
	private String lName;
	private String profileLink;
	private boolean charity;	
	

	public User() {
	}

	public User(String username, String password, boolean enabled) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}

	public User(String username, String password, boolean enabled, Set<UserRole> userRole) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.userRole = userRole;
	}

	@Id
	@Column(name = "username", unique = true, nullable = false, length = 45)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", nullable = false, length = 60)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "enabled", nullable = false)
	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<UserRole> getUserRole() {
		return this.userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}
	
	@Column(name = "email", unique = true, nullable = false, length = 30)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "location", unique = false, nullable = true, length = 50)
	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	@Column(name = "description", unique = false, nullable = true, length = 250)
	public String getDesc() {
		return this.description;
	}

	public void setDesc(String description) {
		this.description = description;
	}
	
	@Column(name = "fName", unique = false, nullable = true, length = 30)
	public String getFName() {
		return this.fName;
	}

	public void setFName(String fName) {
		this.fName = fName;
	}
	
	@Column(name = "lName", unique = false, nullable = true, length = 30)
	public String getLName() {
		return this.lName;
	}

	public void setLName(String lName) {
		this.lName = lName;
	}
	
	
	@Column(name = "profileLink", unique = false, nullable = true, length = 100)
	public String getProfileLink() {
		return this.profileLink;
	}

	public void setProfileLink(String profileLink) {
		this.profileLink = profileLink;
	}
	
	@Column(name = "charity", nullable = false)
	public boolean isCharity() {
		return this.charity;
	}

	public void setCharity(boolean charity) {
		this.charity = charity;
	}

	
	
	//DEANS STUFF
    private Stat stat;

    @OneToOne(mappedBy="user", cascade=CascadeType.ALL)
	public Stat getStat() {
		return stat;
	}

	public void setStat(Stat stat) {
		this.stat = stat;
	}
	
	
    private Set<Event> eventsPledged = new HashSet<Event>();

    @ManyToMany(mappedBy="pledgedUsers")
	public Set<Event> getEventsPledged() {
		return eventsPledged;
	}

	public void setEventsPledged(Set<Event> eventsPledged) {
		this.eventsPledged = eventsPledged;
	}
	
}
