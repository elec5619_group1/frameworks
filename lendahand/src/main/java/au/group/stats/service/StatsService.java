package au.group.stats.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.group.events.model.Event;
import au.group.stats.model.Badge;
import au.group.stats.model.Karma;
import au.group.stats.model.Stat;
import au.group.users.model.User;
import au.group.users.service.UserService;

@Service
@Transactional
public class StatsService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Resource
	private UserService userService;
	
	
	public Stat findByUsername(String username) {
		
		User user = userService.searchUserByUsername(username);
		
		if (user == null)
			return null;
		
		return user.getStat();
	}
	
	public Karma awardKarma (User user, Event event)
	{
		Karma karma = new Karma();
		karma.setPoints(10);
		karma.setEvent(event);
		karma.setStat(user.getStat());
		karma.setTimestamp(new Date());
		
		sessionFactory.getCurrentSession().save(karma);
		
		event.getAwardedUsers().add(karma);
		findByUsername(user.getUsername()).getAwardedCauses().add(karma);
		
		sessionFactory.getCurrentSession().update(event);
		sessionFactory.getCurrentSession().update(user);
		
		return karma;
	}
	
	@Transactional
	public void awardBadge (User user, Badge badge)
	{
		
		Stat stat = findByUsername(user.getUsername());
		badge.getAwardedUsers().add(stat);
		stat.getAwardedBadges().add(badge);
		
		sessionFactory.getCurrentSession().update(stat);
		sessionFactory.getCurrentSession().update(badge);
		
	}
	
	public void createBadge(User user, String name, String svg)
	{
		
		Stat stat = findByUsername(user.getUsername());		
		Badge badge = new Badge(name, svg, stat);
		stat.getBadges().add(badge);
		sessionFactory.getCurrentSession().save(badge);
		sessionFactory.getCurrentSession().save(user);	
		
	}
	
	public Badge getBadgeById(long id)
	{
		List<Badge> badges = new ArrayList<Badge>();

		badges = sessionFactory.getCurrentSession()
				.createQuery("from Badge where id=?").setParameter(0, id)
				.list();

		if (badges.size() > 0) {
			return badges.get(0);
		} else {
			return null;
		}
	}
	
	
	public int getTotalKarma(Stat stat)
	{
		int score = 0;
		for (Karma k : stat.getAwardedCauses())
		{
			score += k.getPoints();
		}
		
		return score;
	}
	
	public int getRecentKarma(Stat stat)
	{
		double score = 0;
		for (Karma k : stat.getAwardedCauses())
		{
			Long awardedTime = k.getTimestamp().getTime();
			Long currentTime = (new Date()).getTime();
			Long elapsed = currentTime - awardedTime;
			int startScore = k.getPoints();
			
			//after 2 weeks an award's value should decrease to 0
			Long effectiveDuration = new Long(1000 * 60 * 60 * 24 * 7 * 2);
			double gradient = (-startScore)/(double)effectiveDuration;
			score += gradient*elapsed + startScore;
		}
		
		return (int) Math.round(score);
	}

}
