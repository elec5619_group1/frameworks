package au.group.stats.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import au.group.users.model.User;

@Entity
@Table(name = "stats", catalog = "lendahand")
public class Stat {
	
    private String userId;

    @Id
    @Column(name="user_id", unique=true, nullable=false)
    @GeneratedValue(generator="gen")
    @GenericGenerator(name="gen", strategy="foreign", parameters=@Parameter(name="property", value="user"))
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	private User user;

    @OneToOne
    @PrimaryKeyJoinColumn
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	private Set<Karma> awardedCauses;
	
	@OneToMany(mappedBy="stat", fetch=FetchType.EAGER)
	public Set<Karma> getAwardedCauses() {
		return awardedCauses;
	}

	public void setAwardedCauses(Set<Karma> awardedCauses) {
		this.awardedCauses = awardedCauses;
	}
	
	private Set<Badge> awardedBadges = new HashSet<Badge>();;
	
	@OneToMany(mappedBy="awardedUsers", fetch=FetchType.EAGER)
	public Set<Badge> getAwardedBadges() {
		return awardedBadges;
	}

	public void setAwardedBadges(Set<Badge> awardedBadges) {
		this.awardedBadges = awardedBadges;
	}
	
	private Set<Badge> badges = new HashSet<Badge>();

	@ManyToMany(mappedBy="owner", fetch=FetchType.EAGER)
	public Set<Badge> getBadges() {
		return badges;
	}

	public void setBadges(Set<Badge> badges) {
		this.badges = badges;
	}

	public Stat(User user)
	{
		setUser(user);
	}
	
	public Stat()
	{

	}
	

}
