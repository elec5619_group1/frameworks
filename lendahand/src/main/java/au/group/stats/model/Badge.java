package au.group.stats.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import au.group.users.model.User;

@Entity
@Table(name = "badges", catalog = "lendahand")
public class Badge {
	
	private String name;
	private String badgeSVG;
	private Long badgeId;
	private Stat owner;
	private Set<Stat> awardedUsers = new HashSet<Stat>();;
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="badge_svg")
	public String getBadgeSVG() {
		return badgeSVG;
	}
	public void setBadgeSVG(String badgeSVG) {
		this.badgeSVG = badgeSVG;
	}
	
	@Id
    @GeneratedValue
    @Column(name="badge_id")
	public Long getBadgeId() {
		return badgeId;
	}
	public void setBadgeId(Long badgeId) {
		this.badgeId = badgeId;
	}
	
	@ManyToOne
    @JoinColumn(name="stat_id")
	public Stat getOwner() {
		return owner;
	}
	public void setOwner(Stat owner) {
		this.owner = owner;
	}
	
	@ManyToMany(targetEntity=Badge.class,
            cascade=CascadeType.ALL)
    @JoinTable(name="badge_stat",      
                inverseJoinColumns={@JoinColumn(name="stat_id")},
				joinColumns={@JoinColumn(name="badge_id")} )
	public Set<Stat> getAwardedUsers() {
		return awardedUsers;
	}
	public void setAwardedUsers(Set<Stat> awardedUsers) {
		this.awardedUsers = awardedUsers;
	}
	
	public Badge(String name, String svg, Stat owner)
	{
		this.setName(name);
		this.setBadgeSVG(svg);
		this.setOwner(owner);
		this.setAwardedUsers(new HashSet<Stat>());
	}
	
	public Badge()
	{
		
	}

}
