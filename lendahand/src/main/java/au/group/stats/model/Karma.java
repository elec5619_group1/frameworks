package au.group.stats.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import au.group.events.model.Event;
import au.group.users.model.User;

@Entity
@Table(name = "karma", catalog = "lendahand")
public class Karma {
	
	
	private Long karmaId;
    private Event event;
    private Stat stat;
	private Date timestamp;
	private int points;

	@Id
    @GeneratedValue
    @Column(name="karma_id")
	public Long getKarmaId() {
		return karmaId;
	}

	public void setKarmaId(Long karmaId) {
		this.karmaId = karmaId;
	}

	@ManyToOne
    @JoinColumn(name="event_id")
	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	@ManyToOne
    @JoinColumn(name="stat_id")
	public Stat getStat() {
		return stat;
	}

	public void setStat(Stat stat) {
		this.stat = stat;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="timestamp", nullable=false)
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	
	@Column(name="karma_award_id")
	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}
	
	public Karma()
	{
	}
}
