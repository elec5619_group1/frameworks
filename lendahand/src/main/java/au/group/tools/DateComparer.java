package au.group.tools;

import java.util.Comparator;

import au.group.events.model.Event;


public class DateComparer implements Comparator<Event> {

	@Override
	public int compare(Event o1, Event o2) {
		return o1.getStartDate().compareTo(o2.getStartDate());
	}
}