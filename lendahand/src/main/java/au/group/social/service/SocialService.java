package au.group.social.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.group.social.dao.UserConnectionDao;

@Service
public class SocialService {
	

	@Autowired
	private UserConnectionDao connectionDao;
	
	private Facebook facebook = new FacebookTemplate("CAANaJgomKg4BAHZCZCZAdjsOHIymf0sv3cz2NybquR8FuZB2fFhqYJBoLIHkVW3uIoFy1rN5sg76trQlqgibaoJaowRpfBSvlCURCT7Rh3fPYZBYX2CsfjdNln2ywqZCHo3mjtT3P52EPoOvstMl5hF01fiXq3bLrTP6ZBU2tF9yJZCHlhuUQyDHJo3ibLmpCBPm7dTopRUlOyzn3i9YbZCw4XVpDvN2NzaUZD"
);

	@Transactional
	public String getToken(String username)
	{
		String tok = connectionDao.getToken(username);
		return tok;
	}
	
	public void setFbInstance(Facebook facebook){
		this.facebook = facebook;
	}
	
	public Facebook getFbInstance(){
		return facebook;
	}
}
