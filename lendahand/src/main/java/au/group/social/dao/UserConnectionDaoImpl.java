package au.group.social.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserConnectionDaoImpl implements UserConnectionDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public String getToken(String username) {
		String tok = sessionFactory.getCurrentSession()
				.createQuery("select accessToken from UserConnection where userId =?")
				.setParameter(0, username).toString();
		return tok;
	}

}
