package au.group.social.dao;

public interface UserConnectionDao {

	String getToken(String username);
}
