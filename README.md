# README #

Okay so this Readme will be slim for now but please keep it updated when you can. We are developing a web app currently called LendAHand.org (but may change in time).

### What is LendAHand? ###

* LendAHand is a website that allows users to volunteer to help other users with a particular cause!

### How do I get set up? ###

* Pull Latest Repo
* Add the Repo as a workspace
* Your STS should create a Servers folder that is your own.
* It is VERY important that you don't commit and push this folder. Make sure to set git/sourcetree to ignore anything in the repo that is NOT in the lendahand folder

* You may not see the lendahand folder in your workspace when you move it to there, just import it as an existing project.
* There may be errors indicated when its imported, those should all be .jsp oriented.


### Setting up Spring Social ###

1. The current spring social implementation is programmed to look for a JAR in a directory that does not exist. The following steps will fix this issue.
1. Locate your Maven artefact repository (Should be in your user/home directory as a folder called .m2)
1. Navigate to the following path .m2/repository/org/springframework/social/
1. Create/Navigate to the folder spring-social-core and make a folder called 1.1.0.BUILD-SNAPSHOT
1. Now go to this URL and download the 1.1.0.RELEASE jar http://mvnrepository.com/artifact/org.springframework.social/spring-social-core into the directory we just created
1. Rename the JAR just downloaded to spring-social-core-1.1.0.BUILD-SNAPSHOT.jar
1. Navigate to the following path .m2/repository/org/springframework/social/
1. Create/Navigate to the folder spring-social-config and make a folder called 1.1.0.BUILD-SNAPSHOT
1. Now go to this URL and download the 1.1.0.RELEASE jar http://mvnrepository.com/artifact/org.springframework.social/spring-social-config into the directory we just created
1. Rename the JAR just downloaded to spring-social-config-1.1.0.BUILD-SNAPSHOT.jar
1. Now you should no longer receive the missing artefact error



### Contribution guidelines ###

To maximize marks we should remember to do a few things like...

* Add to the Wiki
* Create tickets, etc.
* Write tests for your module
* Make it easy for people to implement your module (We'll look into this more later)

### Who do I talk to? ###

* Each other on Facebook group.